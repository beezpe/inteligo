/*
 * Master Page
 * @author mfyance
 */

window.Global = {}

var masterpage = (function () {

  var scopes = {
    
  }

  var subsEvents = function(){
    
    window.addEventListener( 'resize', events.resizeWindow );    
    $('#btnProfile').on("click", events.openProfileMenu);    
    $('#btnOpenMenuMobile').on("click", events.openMenuMobile );    
    $('#inteligoMain').on("click", events.closeMenuMobile );    
    $('#btnMenuVerMas').on("click", events.openMenuVerMas ) ;    
    $('#btnMenuVerMasClose').on("click", events.closeMenuVerMas ) ;
    $("body").on("keyup keypress", '[data-type="email"]', events.controlSpacesEmail ) ;
    $("body").on("keyup keypress", '[data-equal]', events.controlEqual ) ;
   
    // Controlar click en el fondo negro
    $('#layoutBackdrop').on("click", events.controlModalOpen);    
    // [Plugins locales]
    $('[data-event="notify"]').on("click", events.closeNotify );    
    $('[data-tab*="tab-"]').on("click", events.tabs );
  };
  var webServices = {
    
    // Pintar datos en el Menu  del Sidebar
    getSidebar : function(){
      var url = "../../private/organisms/sidebar/mock.json";
      $.ajax({
        url: url,
        success: function(data){
          
        }
      }).done(function() {
        setTimeout(function(){
          Global.closeLoading("#sidebarSkeleton")
          $('#sidebarSkeleton').removeAttr("style")
        }, 1500);
      });
    }
  }
  var events = {
    // Controlar campos similares
    controlEqual: function(){
      var _parent = $(this).attr("data-equal")
      var _idParent = $('#'+_parent).val()

      var _result = ($(this).val() == _idParent) ? true : false
      return _result      
    },  
    // Controlar espacios vacios
    controlSpacesEmail: function(){
      var string = $(this).val();
      $(this).val(string.replace(/ /g, ""))
    },
    // Controlar modales abiertas, cerrarla al dar clic en el bfondo negro
    controlModalOpen: function(){
      var id = $("body").find(".inteligo-modal.open").attr("id");
      Global.modalClose("#"+id);
    },

    // Cerrar en el Ver más en el Menu-Toolbar
    closeMenuVerMas: function () {
      $('body').removeClass("inteligo-mobile-vermas")
      $('#btnMenuVerMas').removeClass("active")
    },
    // Ver más en el Menu-Toolbar
    openMenuVerMas: function(){
      $(".header-profile").removeClass("active")
      $('body').toggleClass("inteligo-mobile-vermas")
      $(this).toggleClass("active")
    },
    // Eventos en los tabs
    tabs : function(){
      $(this).closest(".inteligo-tabs-main").find('[data-tab*="tab-"]').removeClass("inteligo-tab-active")
      if($(this).hasClass("inteligo-tab-active") != true){
        $(this).addClass("inteligo-tab-active")
        var idTab = $(this).data("tab")
        $(this).closest(".inteligo-tabs-main").find('[data-tab-id*="tab-"]').hide()
        $('[data-tab-id="'+idTab+'"]').show()
      }
    },
    // Cuando se actualiza las dimensiones del navegador
    resizeWindow: function () {      
      // Volver a consultar el tamaño del dispositivo
      Global.isWidth = $(window).width();
    },

    // Cerrar las notificaciones
    closeNotify: function(){
      $(this).closest(".inteligo-notify").fadeOut().delay(20000).queue(function() {
        $(this).remove().dequeue();
      });
    },
    // Revisar si hay modales abiertos, al dar clic en el fondo negro o main
    closeMenuMobile: function(){
      var openMenu = $(this).closest("body").hasClass("inteligo-mobile-open");
      var openMenuToolbar = $(this).closest("body").hasClass("inteligo-mobile-vermas");
      var isMobile  = $(this).closest("body").hasClass("inteligo-mobile")
      if(openMenu == true ){
        $("body").removeClass("inteligo-mobile-open")
      }
      if(openMenuToolbar == true){
        $("body").removeClass("inteligo-mobile-vermas")
        $('#btnMenuVerMas').removeClass("active")
      }

      if($('.header-profile').hasClass("active") == true){
        $('.header-profile').removeClass("active")
      }else{
        return true
      }
      event.stopPropagation();
    },
    // Abri el Menu en Mobile
    openMenuMobile: function (event) {
      $('.header-profile').removeClass("active")
      $('body').removeClass("inteligo-mobile-vermas")
      $("body").toggleClass("inteligo-mobile-open");
      $('#btnMenuVerMas').removeClass("active")

      event.stopPropagation();
    },
    // Abrir el Menu del Perfil
    openProfileMenu: function(event){
      $("body").removeClass("inteligo-mobile-open , inteligo-mobile-vermas");
      if($(".header-profile").hasClass("active") === true ){
        $(".header-profile").removeClass("active")
      }else{
        $(this).closest(".header-profile").addClass("active")
      }
      event.stopPropagation();
    }
  };
 
  var methods = {
    // Detectar el tipo de navegador
    detectMobile : function (argument) {
      var size = window.innerWidth;
      if(Global.isMobile() || size < 1280 ){        
        document.getElementsByTagName('body')[0].classList.add("inteligo-mobile")
      }
    },
    // Carga de Plugins Internos
    loadSandBox : function () {
      // Cargar Js de manera dinámica
      Global.loadJS = function (src) {
        var script = document.createElement('script');
        script.src = src;
        script.async = false;
        document.body.append(script);
      },
      // Cargar Css de manera dinámica
      Global.loadCSS = function (href) {
        var link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = href;
        document.head.appendChild(link);
      },
      // Eliminar loading dentro de un contenedor por su ID
      Global.closeLoading = function (id) {
        $(id).removeAttr("style")
        $(id).find(".skeleton").fadeOut()
      },
      // Crear loading dentro de un contenedor por su ID
      Global.showLoading = function (id, size) {
        $(id).attr("style", size)
        $(id).find(".skeleton").fadeIn()
      },
     
      // Detectar móbiles
      Global.isMobile = function(){
        var mobile = false;
        if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
          mobile = true;
        }
        return mobile;
      },
      // Calcular barras del Card2
      Global.calculateBars = function () {
        var card1Header = $('.inteligo-portfolio-risk').outerHeight();
        var card1Middle = $('#loadContentTabs').outerHeight();
        var card1Footer = $('.inteligo-tabs-content').outerHeight();
        var card2Content = 250;

        var sizeBarras = card1Header + card1Middle + card1Footer - card2Content;
        return sizeBarras;        
      },
      // Mostrar Skeleton
      Global.showSkeleton = function (id, error, theme) {
        $(id).find(".skeleton").show()
      },
      // Ocular Skeleton
      Global.hideSkeleton = function (id) {
        $(id).find(".skeleton").hide()
      },
      // Mostrar cuando hay un problema de servicio
      Global.showBrokenCard = function (id, button, design) {
        var tmpBroken = `<div class="broken-service broken-service-${design}" >
          <div class="broken-service-content">
            <div class="broken-service-img">
              <i class="icon icon-broken"></i>
            </div>
            <div class="broken-service-title">No pudimos cargar la información solicitada.</div>
            <button id="${button}" class="button button-primary broken-button" >Volver a intentarlo</button>
          </div>
        </div>`;

        $(tmpBroken).appendTo(id)
      },
      // Ocultar el brokenCard
      Global.hideBrokenCard = function (id){
        $(id).find(".broken-service").remove()
      },
      // Mostrar saludo en los insights
      Global.showSaludo = function (id) {
        var texto = "";
        var ahora=new Date(); 
        var hora=ahora.getHours();
        
        if(hora>=0 ){
          texto="¡Buenos días!"; 
          $(id).addClass("saludo-dia") 
        }
        if(hora>11){ 
          texto="¡Buenas tardes!";
          $(id).addClass("saludo-tarde")
        }
        if (hora>18){ 
          texto="¡Buenas noches!";
          $(id).addClass("saludo-noche")
        }

        $(id).text(texto)

      },
      // Comprobar su es formato de correo
      Global.isMail = function(email){
        var _exp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var _reg = new RegExp(_exp, "i");
        var _status = (_reg.test(email) == true) ? true : false
        return _status
      },
      // Enmascarar el correo
      Global.maskMail = function(email){
        var _email = "";
        var str = email.split('');
        var finalArr=[];
        var len = str.indexOf('@');
        str.forEach((item,pos)=>{
          (pos>=1 && pos<=len-2) ? finalArr.push('*') : finalArr.push(str[pos]);
        })
        var _email = finalArr.join('')
        return _email
      },
      // Ejecutar el selector
      Global.runSelect = function(_id){
        $(_id).niceSelect();
      },
      // Refrescar valor del Select
      Global.reloadSelect = function(_id, _val){
        $(_id).val(_val)
        $(_id).trigger("change")
        $(_id).niceSelect("update");
      },
      Global.isEqual = function(_parent , _child){
        var _result = ($(_parent).val() == $(_parent).val()) ? true : false
        return _result      
      }
    },
    // Efecto clic en los botones
    clicMaterial : function () {
      var ua =navigator.userAgent;
      if(ua.indexOf('iPhone') > -1 || ua.indexOf('iPad') > -1 || ua.indexOf('iPod')  > -1){
        var start = "touchstart";
        var move  = "touchmove";
        var end   = "touchend";
      } else{
        var start = "mousedown";
        var move  = "mousemove";
        var end   = "mouseup";
      }
      var clic_ink, d, x, y;
      $(".clic-material").on(start, function(e){
        if($(this).find(".clic_ink").length === 0){
          $(this).prepend("<span class='clic_ink'></span>");
        }
             
        clic_ink = $(this).find(".clic_ink");
        clic_ink.removeClass("animate");
         
        if(!clic_ink.height() && !clic_ink.width()){
          d = Math.max($(this).outerWidth(), $(this).outerHeight());
          clic_ink.css({height: d, width: d});
        }
         
        x = e.originalEvent.pageX - $(this).offset().left - clic_ink.width()/2;
        y = e.originalEvent.pageY - $(this).offset().top - clic_ink.height()/2;
         
        clic_ink.css({top: y+'px', left: x+'px'}).addClass("animate");
      });
    }
   
  }

  var initialize = function () {
    // Calcular el tamaño del dispositivo
    Global.isWidth = $(window).width();
    methods.loadSandBox()
    methods.detectMobile()
    webServices.getSidebar();
    methods.clicMaterial();
    subsEvents();
  };

  return {
    init: initialize
  };
})();

document.addEventListener(
  'DOMContentLoaded',
  function () {
    masterpage.init();
  },
  false
);




