
 var objToday = new Date(),
 weekday = new Array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'),
 dayOfWeek = weekday[objToday.getDay()],
 domEnder = function() { var a = objToday; if (/1/.test(parseInt((a + "").charAt(0)))) return ""; a = parseInt((a + "").charAt(1)); return 1 == a ? "" : 2 == a ? "" : 3 == a ? "" : "" }(),
 dayOfMonth = today + ( objToday.getDate() < 10) ? '0' + objToday.getDate() + domEnder : objToday.getDate() + domEnder,
 months = new Array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'),
 curMonth = months[objToday.getMonth()],
 curYear = objToday.getFullYear(),
 curHour = objToday.getHours() > 12 ? objToday.getHours() - 12 : (objToday.getHours() < 10 ? "0" + objToday.getHours() : objToday.getHours()),
 curMinute = objToday.getMinutes() < 10 ? "0" + objToday.getMinutes() : objToday.getMinutes(),
 curSeconds = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds(),
 curMeridiem = objToday.getHours() > 12 ? "PM" : "AM";
var today = dayOfWeek + " " + dayOfMonth + " de " + curMonth + ", " + curYear;

document.getElementById('fechadehoy').textContent = today;

$(document).ready(function () {
 


    $("#message_form1").on("keyup", function () {
        $("#message_form1_limit span").empty(), 
        $("#message_form1_limit span").html(350 - this.value.length);

        if(this.value.length < 0){
            $('#contentLimit').addClass("counter-limited")
        }else{
           $('#contentLimit').removeClass("counter-limited")
        }


        var inpCorreo = $('#email_form1').val().trim().length
        var inpAsunto = $('#affair_form1').val().trim().length
        var inpMsj = $('#message_form1').val().trim().length


        if(inpCorreo > 0 && inpAsunto >0 && inpMsj >0 ){
            $('.btnSend').removeClass("disabled")
        }else{
             $('.btnSend').addClass("disabled")
        }

       
    }),
     $('.chatClose, .close').on("click", function(){
        $("#message_form1_limit span").html(350)
     }),

    $(".btnChat").on("click", function () {
        if($('.chatBox').hasClass('chatBoxOpen')){
            $(".chatWelcome").show();
            $(".chatTitle").hide();
            $(".chatBox").toggleClass("chatBoxOpen");
            $(this).toggleClass("btnChatOpen");
            $(".chatStep").hide();
            $('.form__msjerror').remove();
            $('.error').removeClass('error');

        }else{
            $(this).toggleClass("btnChatOpen"),
            $(".chatBox").toggleClass("chatBoxOpen"),
            $(".chatStep").hide(),
            $(".chatStep1").show(),
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style"),
            $(".chatStep1 .messageBox").eq(0).addClass("animate__animated animate__fadeInUp"),
            setTimeout(function () {
                $(".chatStep1 .messageBox").eq(0).find(".typingBox").addClass("animate__animated animate__fadeOut");
            }, 1e3),
            setTimeout(function () {
                $(".chatStep1 .messageBox").eq(0).find(".txt").addClass("animate__animated animate__fadeIn");
            }, 1300),
            setTimeout(function () {
                $(".chatStep1 .messageBox").eq(1).addClass("animate__animated animate__fadeIn");
            }, 1100),
            setTimeout(function () {
                $(".chatStep1 .messageBox").eq(1).find(".typingBox").addClass("animate__animated animate__fadeOut");
            }, 2500),
            setTimeout(function () {
                $(".chatStep1 .messageBox").eq(1).find(".txt").addClass("animate__animated animate__fadeIn");
            }, 2800),
            setTimeout(function () {
                $(".chatStep1 .messageOptionBox").addClass("animate__animated animate__fadeIn");
            }, 3500);
            $('.form__msjerror').remove();
            $('.error').removeClass('error');

        };

        $('#formStep1').trigger("reset");
        $('.select-block .active-list').html('Seleccione su asunto');
        $('#affair_form1').removeAttr('aria-describedby');
        $('#affair_form1').removeAttr('value');
        $(".custom-select .drop-down-list li").click(function () {
            var e = $(this).parent().parent();
            e.removeClass("custom-select-empty"),
            e.parent(".select-block").removeClass("focus").addClass("added"),
                e.find(".active-list").text($(this).text()),
                e.find("input.list-field").attr("value", $(this).text()),
                $(this).closest(".custom-select").removeClass("error"),
                $(this).closest(".content__field").find(".form__msjerror").remove();
        });
       
    }),

    $(".chatClose").on("click", function () {
        $(".chatWelcome").show(),
        $(".chatBox").toggleClass("chatBoxOpen"),
            $(".btnChat").toggleClass("btnChatOpen"),
            $(".chatStep").hide(),
            $(".chatStep1").show(),
            $(".chatTitle").hide(),
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style");

            $('#formStep1').trigger("reset");
            $('.select-block .active-list').html('Seleccione su asunto');
            $('#affair_form1').removeAttr('aria-describedby');
            $('#affair_form1').removeAttr('value');
            $(".custom-select .drop-down-list li").click(function () {
                var e = $(this).parent().parent();
                e.removeClass("custom-select-empty"),
                e.parent(".select-block").removeClass("focus").addClass("added"),
                    e.find(".active-list").text($(this).text()),
                    e.find("input.list-field").attr("value", $(this).text()),
                    $(this).closest(".custom-select").removeClass("error"),
                    $(this).closest(".content__field").find(".form__msjerror").remove();
            });
    }),
    $(".messageOption1").on("click", function () {
        $(".chatStep").hide(),
            $(".chatStep2").show(),
            $(".chatTitle1").show(),
            $(".chatWelcome").hide()
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style"),
            $(".chatStep2 .messageBox").eq(0).css("opacity", 1),
            $(".chatStep2 .messageBox").eq(0).find(".txt").css("opacity", 1),
            setTimeout(function () {
                $(".chatStep2 .formStep").addClass("animate__animated animate__fadeIn");
            }, 200);
    }),
    $(".messageOption2").on("click", function () {
        $(".chatStep").hide(),
            $(".chatStep3").show(),
            $(".chatTitle1").show(),
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style"),
            $(".chatStep3 .messageBox").eq(0).css("opacity", 1),
            $(".chatStep3 .messageBox").eq(0).find(".txt").css("opacity", 1),
            setTimeout(function () {
                $(".chatStep3 .formStep").addClass("animate__animated animate__fadeIn");
            }, 200);
    }),
    $("form").submit(function (e) {
        e.preventDefault();

        let email = $('#email_form1').val();
        let asunto = $('#affair_form1').val();
        let msj = $('#message_form1').val()

        console.log(email + " - " + asunto + " - " + msj)

    }),

  $.validator.addMethod("noSpace", function(value, element) { 
  return value.trim().length > 0 && value != ""; 
}, "No space please and don't leave it empty");


    $.validator.setDefaults({
        errorClass: 'form__msjerror',
        errorElement: 'small',
        focusInvalid: false,
        onkeyup: false,
        
        errorPlacement: function(error, element) {
            if (element.is(':radio')) {
                error.appendTo(element.closest($('.content__field')));
            }
            else if (element.is(':checkbox')) {
                error.appendTo(element.closest($('.content__field')));
            }
            else {
                error.appendTo(element.closest($('.content__field')));
            }
        },
        showErrors: function(errorMap, errorList) {
            console.log('Se presentaros ' + this.numberOfInvalids() + ' errores');
            this.defaultShowErrors();
        },
        unhighlight: function(element, errorClass, validClass) {
            var numberOfInvalids;
            numberOfInvalids = this.numberOfInvalids();
            if (numberOfInvalids === 0) {
                $(element).closest("form").removeClass('form--is-error');
            } else {
                $(element).closest("form").addClass('form--is-error');
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).closest($('.content__field')).removeClass('success').addClass('error');
           
        },
        success: function(element, errorClass, validClass) {
            $(element).closest($('.content__field')).removeClass('error').addClass('success');
        }
    });
    
    $("#formStep1").validate({

        submitHandler: function () {
            $(".chatStep").hide(),
            $(".chatStep4").show(),
            $(".chatTitle").hide(),
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style"),
            $(".chatStep4 .thanksBox").addClass("animate__animated animate__fadeIn");
        },
        highlight: function (e) {
            $(e).parent("div").addClass("error");
        },
        unhighlight: function (e) {
            $(e).parent("div").removeClass("error");
        },
        rules: { 
            email_form1: 
                { 
                    required: !0, 
                    email: !0 
                }, 
            affair_form1: "required",
            message_form1: {
                required: true,
                noSpace: true
            }
         },
        messages: { 
            email_form1: "Ingrese un correo válido por favor.", 
            affair_form1: "Seleccione el asunto por favor.",
            message_form1: "Deje un mensaje por favor." 
         },
    }),
    $("#formStep2").validate({
        errorElement: "small",
        rules: { phone_form2: { required: !0, minlength: 6, maxlength: 14, number: !0 }, affair_form2: "required", message_form2: "required" },
        submitHandler: function () {
            $(".chatStep").hide(),
                $(".chatStep4").show(),
                $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
                $(".chatStep").find("*").removeAttr("style"),
                $(".chatStep4 .thanksBox").addClass("animate__animated animate__fadeIn");
        },
        highlight: function (e) {
            $(e).parent("div").addClass("error");
        },
        unhighlight: function (e) {
            $(e).parent("div").removeClass("error");
        },
        messages: { phone_form2: "Ingrese un teléfono valido por favor.", affair_form2: "Seleccione el asunto por favor.", message_form2: "Deje un mensaje por favor." },
    }),
    $(".btnThanks1").on("click", function () {
        $(".chatStep").hide(),
            $(".chatStep5").show(),
            $(".chatTitle").hide(),
            $(".chatTitle2").show(),
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style"),
            $(".chatStep5 .messageBox").eq(0).addClass("animate__animated animate__fadeIn"),
            setTimeout(function () {
                $(".chatStep5 .messageBox").eq(0).find(".typingBox").addClass("animate__animated animate__fadeOut");
            }, 800),
            setTimeout(function () {
                $(".chatStep5 .messageBox").eq(0).find(".txt").addClass("animate__animated animate__fadeIn");
            }, 1e3),
            setTimeout(function () {
                $(".chatStep5 .formStep").addClass("animate__animated animate__fadeIn");
            }, 1700);
    }),
    $("#formStep3").validate({
        errorElement: "small",
        rules: { message_form3: "required" },
        submitHandler: function () {
            $(".chatBox").toggleClass("chatBoxOpen"),
            $(".chatStep").hide(),
            $(".chatStep1").show(),
            $(".btnChat").toggleClass("btnChatOpen"),
            $(".chatTitle").hide(),
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style");

            $("#email_form1").val("");
            $('#message_form1').val("")
        },
        messages: { message_form3: "Deje un mensaje por favor." },
    }),
    $(".btnThanks2").on("click", function () {
        $(".chatBox").toggleClass("chatBoxOpen"), 
        $(".chatWelcome").show(), 
        $(".chatStep").hide(), 
        $(".chatStep1").show(), 
        $(".btnChat").toggleClass("btnChatOpen"),
         $(".chatTitle").hide();
    }),
    $(".chatTitle1").on("click", function () {
        $(".chatStep").hide(),
            $(".chatWelcome").show(),
            $(".chatStep1").show(),
            $(".chatTitle").hide(),
            $(".chatBox").find(".animate__animated").removeClass("animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut"),
            $(".chatStep").find("*").removeAttr("style"),
            $(".chatStep1 .messageBox").eq(0).css("opacity", 1),
            $(".chatStep1 .messageBox").eq(0).find(".txt").css("opacity", 1),
            $(".chatStep1 .messageBox").eq(0).find(".typingBox").css("opacity", 0),
            $(".chatStep1 .messageBox").eq(1).css("opacity", 1),
            $(".chatStep1 .messageBox").eq(1).find(".txt").css("opacity", 1),
            $(".chatStep1 .messageBox").eq(1).find(".typingBox").css("opacity", 0),
            $(".chatStep1 .messageOptionBox").css("opacity", 1);
    });
})




var e = $(".material-form .floating-field").find(".form-control"),
    a = $(".material-form .input-block").find(".form-control");
a.focus(function () {
    $(this).parent(".input-block").addClass("focus");
}),
a.blur(function () {
    $(this).parent(".input-block").removeClass("focus");
}),
e.each(function () {
    var e = $(this).parent();
    $(this).val() && $(e).addClass("has-value");
}),
e.blur(function () {
    $(this).parent(".input-block").removeClass("focus"), $(this).val().length;
}),
$("body").click(function () {
    $(".custom-select .drop-down-list").is(":visible"), 
    $(".custom-select .drop-down-list:visible").slideUp();
}),
$(".custom-select .active-list").click(function () {
    $(this).parent().parent().addClass("focus"), 
    $(this).parent().find(".drop-down-list").stop(!0, !0).delay(10).slideToggle(300);
}),
$(".custom-select .drop-down-list li").click(function () {
    var e = $(this).parent().parent();
    e.removeClass("custom-select-empty"),
    e.parent(".select-block").removeClass("focus").addClass("added"),
        e.find(".active-list").text($(this).text()),
        e.find("input.list-field").attr("value", $(this).text()),
        $(this).closest(".custom-select").removeClass("error"),
        $(this).closest(".content__field").find(".form__msjerror").remove();
});




   