/*
 * Modal
 * @author Mfyance
 */

var modal = (function () {
  var scopes = {
    mainModals: $('#inteligoContentModals')
  };


  var services = {
    // Generar Modal de Ayuda
    loadModalAyuda : function(){      
      var tmpHelp = {
        "id": "inteligo-help",        
        "modalContent": {
          "title": "Contacte con su asesor",
          "desc": "<p>Detalle su consulta para que su Asesor Financiero e comunique con usted a la brevedad posible.</p>",
          "value": "",
          "icon" : "icon-modal-contact",
          "button2": "Atrás",
          "button2link": "close",
          "button1": "Enviar mensaje",
          "button1link": "send"
        },
        "modalSuccess" : {                  
          "title": "Mensaje enviado con éxito",
          "desc" : "<p>Estimado cliente, la información solicitada ha sido enviada con éxito a su correo.</p>"
        }
      }
      Global.modalInitialize(1,tmpHelp)
    },
    // Gerar Modal de Contacto
    loadModalContactar : function(){
      var tmpContact = {
        "id": "inteligo-contact",        
        "modalContent": {
          "title": "Contacte con su asesor",
          "desc": "<p>Estimado cliente, para conocer más acerca de las ventajas y beneficios de tener su portafolio alineado, y cómo ello le ayuda con el logro de sus objetivos, contacte por este medio a su Asesor y se comunicará con usted a la brevedad posible.</p>",
          "icon" : "icon-modal-contact",
          "button2": "Atrás",
          "button2link": "close",
          "button1": "Enviar mensaje",
          "button1link": "send"
        },
        "modalSuccess" : {                  
          "title": "Mensaje enviado con éxito",
          "desc" : "<p>Su asesor se comunicará a la brevedad con usted para solucionar y/o conversar de cualquier imprevisto que ha podido surgir.</p>"
        }
      }
      // Crear modal de ayuda
      Global.modalInitialize(1,tmpContact)
    },
    // Generar Modal de Envío de Email
    loadModalEmail: function(){
      var tmpEmail = {
        "id": "inteligo-email",        
        "modalContent": {
          "title": "Enviar mail",
          "icon" : "icon-modal-contact",
          "value" : "Estimado ,",
          "button1": "Continuar",
          "button1link": "send"
        },
        "modalSuccess" : {                  
          "title": "Mensaje enviado con éxito",
          "desc" : "<p>Estimado cliente, la información solicitada ha sido enviada con éxito a su correo.</p>"
        }
      }

      // Validaciones en los inputs
      var oExtra = function(){
        $('#inpSendmailCorreo, #inpSendmailAsunto, #sendmailMessage').on("keyup keypress", function(){
          var _correo     = $('#inpSendmailCorreo').val();
          var _asunto     = $('#inpSendmailAsunto').val();
          var _msj        = $('#sendmailMessage').val();
          var _msjError   = ($('#sendmailMessage').val().length > Global.maxCharacter ) ? false : true
          var _mailValid  = Global.isMail(_correo)
          
          setTimeout(function(){
            if(_correo != "" && 
              _asunto != "" && 
              _msj != "" && 
              _msjError == true && 
              _mailValid == true) {
              $('[data-modal="template-8"]').find('[data-modal="send"]').removeAttr("disabled")
            }else{
              $('[data-modal="template-8"]').find('[data-modal="send"]').attr("disabled", true)
            }
          }, 50)          
        })
      }
      // Crear modal de ayuda
      Global.modalInitialize(8,tmpEmail, oExtra)
    }
  }

  var subsEvents = function(){  
    $(scopes.mainModals).on("keyup keypress", ".inteligo-modal-textarea", events.controlMsjAyuda );
    $(scopes.mainModals).on("click", "[data-modal='close']", events.modalResetContent)
    $(scopes.mainModals).on("click", "[data-modal='next']", events.modalControlSteps)
    $(scopes.mainModals).on("click", "[data-modal='prev']", events.modalControlSteps)
    $(scopes.mainModals).on("click", "[data-modal='send']", events.modalSendSuccess)
    $(scopes.mainModals).on("click", "[data-modal='scroll']", events.modalScrollPage)
    // Eventos para Template7
    $(scopes.mainModals).on("click", "[data-modal='skip']", events.modalControlSteps)
    $(scopes.mainModals).on("click", "[data-modal='contact']", events.modalContactSuccess)
    // Evento de Editar Email  Template 9
    $(scopes.mainModals).on("click", "[data-modal='edit']", events.modaEditEmail)
    // Enviar email por medio del componente de Buttons-actions
    $("body").on("click", "[data-modal='email']", events.modalSendEmail)

    // Abrir la modal de ayuda
    $("#btnOpenModalAyuda").on("click", function () {
      Global.modalOpen("#modal-inteligo-help")
    });

    // Cerrar modales dinámicas
    $(scopes.mainModals).on("click", "[data-modal='close']", function(){
      var idModal = $(this).closest('.inteligo-modal').attr("id")
      Global.modalClose("#"+idModal)
    })

  };

  var events = {
    modalSendEmail: function(){
      Global.modalOpen("#modal-inteligo-email")
    },
    modalControlSteps: function (){
      // Detectar la direccion de navegación
      var action = $(this).attr("data-modal")

      // Validar si son pasos consecutivos o un salto de paso
      if(action == "skip"){
        var direction = $(this).attr("data-modal-skip")
        // Ocultar y mostrar la seccion de la modal
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
        $(this).closest(".inteligo-modal").find('[data-modal="'+direction+'"]').show()
      }else{
        // Controlar los pasos dentro de la modal
        var step = $(this).closest(".inteligo-modal-main").attr("data-modal");     
        var id = step.slice(-1);       
        var direction = (action == "prev") ? parseInt(id)-1 : parseInt(id)+1;

        // Ocultar y mostrar la seccion de la modal
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
        $(this).closest(".inteligo-modal").find('[data-modal="step-'+direction+'"]').show()
      }

      // Una casuistica para cuando sea de template 7
      var _template = $(this).closest(".inteligo-modal").attr("data-modal")
      if(_template == "template-7"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main")
      }
      
      // Ejecutar método de resetear posición de scroll
      methods.resetInterScroll()
    },
  
    // Controlar cant de caracteres en la Modal de Ayuda
    controlMsjAyuda: function () {
      Global.maxCharacter = parseInt($(this).closest(".inteligo-modal-main").find(".inteligo-modal-count-number:last-child").text());
      var characterCount = $(this).val().length;
      var current = $(this).closest(".inteligo-modal-main").find(".inteligo-modal-count-number:first-child");
      var textarea = $(this).val().trim().length;
      current.text(characterCount); 

      var tmpModal = $(this).closest(".inteligo-modal").attr("data-modal");
      
      // El template 8, tiene sus propias validaciones
      if(tmpModal != "template-8"){
        if (textarea > 0 && textarea != "") {
          
          $(this).closest(".inteligo-modal-main").find("[data-modal='send']").removeAttr("disabled")
          $(this).closest(".inteligo-modal-main").find("[data-modal='contact']").removeAttr("disabled")

          if ( characterCount > Global.maxCharacter){
            $(this).closest(".inteligo-modal-main").addClass("error")
            $(this).closest(".inteligo-modal-main").find("[data-modal='send']").attr("disabled", true)
            $(this).closest(".inteligo-modal-main").find("[data-modal='contact']").attr("disabled", true)
          }else{
            $(this).closest(".inteligo-modal-main").removeClass("error")
            $(this).closest(".inteligo-modal-main").find("[data-modal='send']").removeAttr("disabled")
            $(this).closest(".inteligo-modal-main").find("[data-modal='contact']").removeAttr("disabled")
          }
        } 
        else {
          $(this).closest(".inteligo-modal-main").removeClass("error")
          $(this).closest(".inteligo-modal-main").find("[data-modal='send']").attr("disabled", true)
          $(this).closest(".inteligo-modal-main").find("[data-modal='contact']").attr("disabled", true)          
        }
      }
      else{
        // Validaciones de caracteres para Template 8
        if (textarea > 0 && textarea != "") {
          if ( characterCount > Global.maxCharacter){
            $(this).parent().addClass("error")
          }else{
            $(this).parent().removeClass("error")
          }
        } 
        else {
          $(this).parent().removeClass("error")          
        }
      }
    },
    // Cerrar modal correspondiente
    modalResetContent : function () {
      var idModal = $(this).closest(".inteligo-modal").attr("id");
      var idTemplate = $(this).closest(".inteligo-modal").attr("data-modal");

      // Ejecutar eventos por template
      if(idTemplate == "template-1"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").show()
      }
      else if(idTemplate == "template-2"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main[data-modal='step-1']").show()
      }
      else if(idTemplate == "template-3"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").show()
      }
      else if(idTemplate == "template-5"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main[data-modal='step-1']").show()
      }
      else if(idTemplate == "template-6"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main[data-modal='step-1']").show()
      }
      else if(idTemplate == "template-7"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-contacting").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-calendars").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main[data-modal='step-1']").show()
      }
      else if(idTemplate == "template-8"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").show()
      }
      else if(idTemplate == "template-9"){
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").hide()
        $(this).closest(".inteligo-modal").find(".inteligo-modal-main[data-modal='step-1']").show()
      }

      // Limpiar al enviar datos del textarea 
      methods.resetControlMsjModal(idModal, idTemplate)

      // Ejecutar método de resetear posición de scroll
      methods.resetInterScroll()

      Global.modalClose("#"+idModal)
    },
    // Mensaje exitoso
    modalSendSuccess: function(){      
      $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
      $(this).closest(".inteligo-modal").find(".inteligo-modal-sending").show()

      // Hacer un mapeo de la data obtenida en la modal
      var _idModal = $(this).closest(".inteligo-modal").attr("id")
      methods.getDataModal(_idModal)

      // Ejecutar método de resetear posición de scroll
      methods.resetInterScroll()
    },
    // Contacto exitoso
    modalContactSuccess: function(){      
      $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
      $(this).closest(".inteligo-modal").find(".inteligo-modal-contacting").show()

      // Hacer un mapeo de la data obtenida en la modal
      var _idModal = $(this).closest(".inteligo-modal").attr("id")
      methods.getDataModal(_idModal)

      // Ejecutar método de resetear posición de scroll
      methods.resetInterScroll()
    },
    // Editar Email
    modaEditEmail: function(){
      $(this).closest(".inteligo-modal").find(".inteligo-modal-main").hide()
      $(this).closest(".inteligo-modal").find(".inteligo-modal-editing").show()
    },
    // Scrollear pantalla
    modalScrollPage: function(){
      var size = window.innerWidth;
      var type  = $(this).attr("data-modal");
      var scroll  = $(this).attr("data-scroll");
      var idModal = $(this).closest(".inteligo-modal").attr("data-id");

      // Scrollear al punto indicado
      if (Global.isMobile() == true || size < 1280 ){
        var idSection = parseInt($(scroll).position().top) - 180
        $("html, body").animate({
          scrollTop: idSection
        }, 100);
      }
      else{
        var idSection = parseInt($(scroll).position().top) - 45
        $("#inteligoMain").animate({
          scrollTop: idSection
        }, 100);
      }

      // Cerrar la modal
      Global.modalClose('#modal-'+idModal)
    }

  };

  var methods = {
    // Crear Sandbox para Modales
    loadSandBox : function () {
      // Método para iniciar la creación de una Modal
      Global.modalInitialize = function(oIdTemplate, oData, oExtra) {
        // Detectar el tipo de template a crear
        switch(oIdTemplate) {
          case 1:
            $.get('../../private/pages/modals/modal-template-1.html',
              function (_template1, _textStatus, _jqXhr) {
                var htmlTmp1 = Mustache.render(_template1, oData);                 
                $(scopes.mainModals).append(htmlTmp1);
            })
            break;
          case 2:
            $.get('../../private/pages/modals/modal-template-2.html',
              function (_template2, _textStatus, _jqXhr) {
                var htmlTmp2 = Mustache.render(_template2, oData);                 
                $(scopes.mainModals).append(htmlTmp2);
            })
            break;
          case 3:
            $.get('../../private/pages/modals/modal-template-3.html',
              function (_template3, _textStatus, _jqXhr) {
                var htmlTmp3 = Mustache.render(_template3, oData);                 
                $(scopes.mainModals).append(htmlTmp3);
                // Pintar graficos que se visualizan en esta plantilla de modal
                methods.printGraphicsTemplate3(oData, oExtra)
            })
            break;
          case 4:
            $.get('../../private/pages/modals/modal-template-4.html',
              function (_template4, _textStatus, _jqXhr) {
                var htmlTmp4 = Mustache.render(_template4, oData);                 
                $(scopes.mainModals).append(htmlTmp4);
                // Pintar graficos que se visualizan en esta plantilla de modal
                methods.printGraphicsTemplate4(oData, oExtra)
            })
            break;
          case 5:
            $.get('../../private/pages/modals/modal-template-5.html',
              function (_template5, _textStatus, _jqXhr) {
                var htmlTmp5 = Mustache.render(_template5, oData);                 
                $(scopes.mainModals).append(htmlTmp5);
            })
            break;
          case 6:
            $.get('../../private/pages/modals/modal-template-6.html',
              function (_template6, _textStatus, _jqXhr) {
                var htmlTmp6 = Mustache.render(_template6, oData);                 
                $(scopes.mainModals).append(htmlTmp6);
            })
            break;
          case 7:
            $.get('../../private/pages/modals/modal-template-7.html',
              function (_template7, _textStatus, _jqXhr) {
                var htmlTmp7 = Mustache.render(_template7, oData);                 
                $(scopes.mainModals).append(htmlTmp7);
                if($.isFunction(oExtra)){
                  oExtra()
                } 
            })
            break;
          case 8:
            $.get('../../private/pages/modals/modal-template-8.html',
              function (_template8, _textStatus, _jqXhr) {
                var htmlTmp8 = Mustache.render(_template8, oData);                 
                $(scopes.mainModals).append(htmlTmp8);
                if($.isFunction(oExtra)){
                  oExtra()
                } 
            })
            break;
          case 9:
            $.get('../../private/pages/modals/modal-template-9.html',
              function (_template9, _textStatus, _jqXhr) {
                var htmlTmp9 = Mustache.render(_template9, oData);                 
                $(scopes.mainModals).append(htmlTmp9);
                if($.isFunction(oExtra)){
                  oExtra()
                } 
            })
            break;
            case 10:
            $.get('../../private/pages/modals/modal-template-10.html',
              function (_template10, _textStatus, _jqXhr) {
                var htmlTmp9 = Mustache.render(_template10, oData);                 
                $(scopes.mainModals).append(htmlTmp9);
                if($.isFunction(oExtra)){
                  oExtra()
                } 
            })
            break;
          default:          
            console.log("Sin template")
        }        
      };
      // Método para abrir una Modal
      Global.modalOpen = function (id) {
        $(id).addClass("open");
        $('#layoutBackdrop').addClass("open");
        $('html , body, .inteligo-layout').addClass("is-hidden");
        // Recalcular la cantidad de caracteres en cada Textarea
        if($(id).find(".inteligo-modal-textarea").length > 0){
          $(id).find(".inteligo-modal-textarea").trigger("keypress");
        }
      };
      // Método para cerrar una Modal
      Global.modalClose = function (id) {
        $(id).removeClass("open");
        $('#layoutBackdrop').removeClass("open");
        $('html, body, .inteligo-layout').removeClass("is-hidden");
        // Volver a mostrar el valor en el textarea
        if($(id).find(".inteligo-modal-textarea").length > 0){
          var _valTxt = $(id).find(".inteligo-modal-textarea").attr("data-value");
          $(id).find(".inteligo-modal-textarea").val(_valTxt);
        }

        // Eliminar asunto de Modal Email
        Global.modalAsuntoHide()

        // En caso haya un objeto de insigth abierto
        Global.oInsight = []
      };
      // Método para pintar Asunto dentro de Modal al Enviar Email
      Global.modalAsuntoShow = function(text){
        $('#inpSendmailAsunto').val(text)
      };
      // Método para limpiar Asunto dentro de Modal al Enviar Email
      Global.modalAsuntoHide = function(){
        $('#inpSendmailAsunto').val("")
      }
      // Método para refrescar data que viene del servicio
      Global.modalReload = function(){
        
      }
    },
    printGraphicsTemplate4: function(oData, oConfig) {
      // Nombre de ID para renderizar grafico
      var _graphicId = "graphic-"+oData.id;

      // Setear datos de config para el grafico
      oConfig.colors = [oData.items[0].color, oData.items[1].color]
      oConfig.tooltip =  {  "enabled": false , "animation": false }

      // Renderiza grafico dentor de la modal
      Global.AnalisisVerMas = Highcharts.chart(_graphicId, oConfig);
      Global.AnalisisVerMas.series[0].update({
        data: [ [oData.items[0].name,oData.items[0].percent],[oData.items[1].percent,oData.items[1].percent]]
      })
    },
    printGraphicsTemplate3: function (oData) {
      // Detectar lugar donde renderiza el grafico dinamico
      var _graphicId = "graphic-"+oData.id
      // Crear array con los colores
      var _graphicColors = []
      // Crear array con la info
      var _graphicLabels = []

      // Controlar que haya data para pintar la gráfica
      if(oData.hasOwnProperty('modalContent2')){
        for( var i=0; i < oData.modalContent2.length; i++){        
          _graphicColors.push(oData.modalContent2[i].color)
          var temp = [oData.modalContent2[i].title,oData.modalContent2[i].percent]
          _graphicLabels.push(temp)
        }
        
        // Configuracion del gráfico
        var DesemPortfolioOptions = {
          title: { text: "" },
          subtitle: { text: "" },
          exporting: { enabled: false },
          chart: { 
            type: "pie", 
            polar: false, 
            height: 220, // ALTO DEL CONTENEDOR DEL GRÁFICO
            width: 370,  // ANCHO DEL CONTENEDOR DEL GRÁFICO
            alignTicks: true, 
            animation: false, 
            ignoreHiddenSeries: true,
            options3d: { 
              frame: { 
                right: { 
                  visible: false 
                } 
              }, 
              fitToPlot: true 
            } 
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: true,
              innerSize: "70%", // TAMAÑO DEL HOLE DE LA DONA
              dataLabels: {
                enabled: true,
                overflow: "visible",
                crop: false,
                padding: 5,
                verticalAlign: "middle",
                inside: true,
                style: { 
                  textOutline: "", 
                  color: "", 
                  fontSize: "11px" 
                },
                distance: 18,
                connectorColor: "#313334", // COLOR DE LOS CONECTORES
                connectorWidth: 1,
                useHTML: true,
                softConnector: false, // NO CAMBIAR O LAS LINEAS SE VOLVERAN CURVAS (EN VALORES ENTEROS A PARTIR DE 0)
                x: 1,
                y: -6,
                defer: true,
                connectorPadding: 1,
                allowOverlap: true,
                shadow: false,
                rotation: 0,
                className: "insights-pie-label",
                format: '<div style="padding: 0px; width: 30px;"><b style="display: flex; justify-content: center;  text-align: center;font: 14px var(--font-primary-medium);" >{point.y}%</div>',
              },
              size: 160, // TAMAÑO DEL LA DONA EN PIXELES 
              center: ["50%", "50%"],
              enableMouseTracking: false,
              startAngle: 325,
              dataGrouping: { 
                groupPixelWidth: 2, 
                smoothed: false 
              },
              label: { 
                style: { 
                  fontWeight: "600" 
                }, 
                connectorAllowed: false, 
                enabled: true, 
                connectorNeighbourDistance: 24, 
                onArea: true 
              },
              states: { 
                hover: { 
                  enabled: false 
                } 
              },
              tooltip: { 
                animation: 
                false 
              },
              lineWidth: 2,
            },
            series: { 
              dataLabels: { 
                enabled: true, 
                style: { 
                  color: "contrast", 
                  fontSize: "12px", 
                  fontWeight: "bold", 
                  textOutline: "" 
                } 
              } 
            },
          },
          series: [
            {
              name: "null",
              turboThreshold: 0,
              dataLabels: { backgroundColor: "transparent", 
              color: "#313334", 
              padding: 5, 
              allowOverlap: false, 
              crop: true, rotation: 0 
            },
              label: { 
                onArea: false, 
                connectorAllowed: true, 
                connectorNeighbourDistance: 24, //TAMAÑO DE LAS LINEAS QUE CONECTAN EL GRÁFICO
                enabled: true 
              },
            }
          ],
          credits: { 
            enabled: false, 
            text: "", 
            href: "" 
          },
          tooltip: { 
            enabled: false, 
            headerFormat: '<span style="font-size: 10px">{point.key}</span><br/>' },
          pane: { 
            background: [] 
          },
          responsive: { 
            rules: [] 
          },
          legend: { 
            enabled: false 
          },
          colors: _graphicColors
        };

        // Renderizar Gráfico 
        var DesemPortfolio = Highcharts.chart(_graphicId, DesemPortfolioOptions);
        DesemPortfolio.series[0].update({
          data: _graphicLabels
        })
      }
    },
    // Resetear los mensajes enviado desde las modales
    resetControlMsjModal: function(_id , _template){
      $("#"+_id).find(".inteligo-modal-count-number:first-child").text("0");
      $("#"+_id).removeClass("error");

      $('#'+_id +' *').filter('select').each(function (i,e) { 
        $(this).val("0");
        $(this).niceSelect('update');
      })
      // Verificar
      $('#'+_id +' *').filter('input:not(.modal-input-config)').each(function (i,e) { 
        $(this).val("");
      })

    },
    // Resetear posicion del scroll en modales internas
    resetInterScroll: function(){
      $(".inteligo-modal-temp2-content, .inteligo-modal-maxh").animate({
        scrollTop: 0
      }, 100);
    },
    // Obtener la data que se envía en cada MODAL
    getDataModal: function(_id){
      Global.oDataModal = []
      $('#'+_id +' *').filter('input, textarea, select').each(function (i,e) {    
        if($(this).attr("name") != undefined ){
          Global.oDataModal.push({
            value : $(this).val(),
            name :$(this).attr("name")
          })
        }
      })
      console.log('----Data de: "'+_id+'"--------')
      console.log(Global.oDataModal)
    }
  }
    

  var initialize = function () {
    methods.loadSandBox()
    services.loadModalAyuda()
    services.loadModalContactar()
    services.loadModalEmail()
    subsEvents();
    // Enable scrolling.
    document.ontouchmove = function (e) {
      if($(e.target).attr("id")== "layoutBackdrop"){
        $('.inteligo-modal.open').find(".inteligo-modal-close").trigger("click")
      }
    }
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    modal.init();
  },
  false
);

