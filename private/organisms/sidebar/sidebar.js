//=========== CUANDO NO EXISTE LA SECCIÓN (.sidebar-block-main) SE AGREGAN ESTAS 2 CLASES

if (!$('.sidebar-block-main').length) {
  $('.sidebar-block-profile').addClass('noprincipal-assets');
  $('.sidebar-block-profile-portfolios').addClass('just-portfolios');
}

// FUNCION PARA INTERACCIÓN DE LOS BOTONES DEL PERFIL DEL SIDEBAR

$('.sidebar-perfil-tabs-href').each(function () {
  $(this).click(function (e) {
    let button = e.currentTarget;
    $('.sidebar-perfil-tabs-href').not(button).removeClass('active');
    $(button).toggleClass('active');
  });
});

//   FIN DE LA INTERACCIÓN

// LE QUITA EL BORDE INFERIOR AL ULTIMO ELEMENTO DE JUST-CASH

$('.portfolio-active-subitems').each(function () {
  $(this).find('.portfolio-active-subitems-head .just-cash').last().css('border-bottom', '0');
  if ($(this).find('.portfolio-active-subitems-head .just-cash').length > 1) {
      $(this).find('.portfolio-active-subitems-head .just-cash').not(':last').css('border-bottom', '1px solid #52627A');
  } else {
      $(this).find('.portfolio-active-subitems-head .just-cash').css('border-bottom', '0');
  }
});


// ESTO AGREGA UN PADDING-LEFT: 12 CUANDO HAY UN CASH O EQUIVALENTE DE CASH
$('.portfolio-active-subitems-head').find('.cash-title').add('.equal-title').siblings('.just-cash, .sidebar-subblock-secondary-2').css('padding-left', '12px');
$('.head-portfolio-active').siblings('.portfolio-active-subitems').find('.activos-level').css('padding-left', '12px');

// si no hay cabeceras de titular o firmante

$(document).ready(function () {
  // Verifica si el elemento anterior a ".sidebar-block-main" tiene la clase ".sidebar-block-profile"
  if (!$('.sidebar-block-main').nextAll().hasClass('sidebar-block-profile')) {
    // Agrega 16 píxeles de padding superior a ".sidebar-block-profile-portfolios"
    $('.sidebar-block-profile-portfolios').css('padding-top', '16px');
  }
});


/*
 * Sidebar
 * @author Mfyance
 */

var sidebar = (function () {
  var scopes = {
    inteligoMain     : "#inteligoMain",
    allCards         : "#secMainCards",
    insights         : "#secInsights",
    ctaTrading       : "#secCtaTrading",
    allTrading       : "#secAllTradings",
    creditCard       : "#secCreditCard",
    detailPage       : "#secDetailpage",
    ctaCorriente     : "#secCtaCorriente",
    allCorrientes    : "#secAllCorrientes",
    prestamos        : "#secPrestamos",
    allPrestamos     : "#secAllPrestamos"
  };
  var subsEvents = function(){ 
    // Event: Configuraciones para Cta Corriente Independiente
    $('body').on("click", '[data-menu-corriente-ind]', methods.configNavCorrienteInd);
    $('body').on("click", '[data-menu-trading]', events.refreshIdTrading);

    // Event: Refrescar ID de Cta Corriente
    $('body').on("click", '[data-corriente]', events.refreshIdCorriente);
    $('body').on("click", '[data-menu-corriente]', events.refreshIdCorriente);

    // Event: Resetear estados "active" del Sidebar
    $('body').on("click", '[data-corriente]', methods.resetSidebarCorriente);
    $('body').on("click", '#secCtaCorriente .cardbox-back-link', methods.resetSidebarCorriente);
    $('body').on("click", '#secAllCorrientes .cardbox-back-link', function(){
      // Abrir la vista de la Cta Corriente anterior
      methods.menuPortafolio(Global.idPortafolio)
    });

    // Event: Clic en el boton de Todos las Ctas Corrientes
    $('body').on("click", '#btnShowAllCorrientes', function(){
      // Ocultar estado activo de los Acordeones de Portafolios
      methods.resetMenuFirst()     
    });

    // Event: Configuraciones para Cta Tradings
    $('body').on("click", '[data-section*="secCtaTrading"]', function(){
      // Abrir la vista de la Cta Corriente anterior
      methods.menuTrading(Global.idTrading)
    });
    $('body').on("click", '[data-section*="secAllTradings"]', methods.resetMenuFirst);
    $('body').on("click", '[data-trading]', methods.resetSidebarTrading);
    
    // Event: Configuraciones para Préstamos
    $('body').on("click", '[data-section*="secAllPrestamos"]', methods.configPrestamos);
    $('body').on("click", '[data-section*="secPrestamos"]', methods.configPrestamos);

    // Event: Configuraciones para Patrimonio
    $('#menuPatrimonioTotal .sidebar-mainoption').on("click", events.openPatrimonio);
    $('#menuCreditCard, #menuPrestamos').on("click", events.closePatrimonio);

    // Event: Clic en cada portafolio del sidebar para regresar al Home
    $('body').on("click", '*[data-portafolio]', events.returnHome);
    $('body').on("click", '*[data-portafolio]', events.configPortafolio);
    
    // Event: Clic en "data-section" del sidebar para regresar al Home
    $("body").on("click", '[data-section*="home"]', events.returnHome);

    // Event: Clic en cada "data-section=SEC" del sidebar para abrir su VISTA
    $("body").on("click", '[data-section*="sec"]', events.openSection);

    // Event: Clic en Sidebar de tipo First y Second
    $("body").on("click", '[data-menu="first"]', events.openMenuFirst);
    $("body").on("click", '[data-menu="second"]', events.openMenuSecond);
  };

  var events = {
    configPortafolio: function(event){
      Global.idPortafolio = $(event.target).closest('[data-menu="first"]').attr("data-portafolio")
      console.log("Id Portafolio" + Global.idPortafolio)
    },
    refreshIdTrading: function(event){
      Global.idPortafolio = $(event.target).closest(".block-profile-portfolios-main").find('[data-menu="first"]').attr("data-portafolio")
      console.log("Id Portafolio" + Global.idPortafolio)
    },
     // Obtener el ID de la Cta Corriente donde hice clic
    refreshIdCorriente: function(){
      // Detectar el ID del detalle
      if($(this).attr("data-menu-corriente")){
        Global.idCorriente = $(this).attr("data-menu-corriente")
       
      }else{
        Global.idCorriente = $(this).attr("data-corriente")
      }      
      console.log("Id Corriente: "+Global.idCorriente)
    },
    // Contraer la opcion de Patrimonio
    closePatrimonio: function(){
      $('#menuPatrimonioTotal .sidebar-mainoption').removeClass("active");
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().removeClass("active")
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().find(".sidebar-mainoption-second").hide()
    },
    // Estilos "active" para Patrimonio y Mostrar/Ocultar contenido
    openPatrimonio: function(){
      if($(this).hasClass("active")){
        $(this).removeClass("active");
        $(this).parent().removeClass("active")
        $(this).parent().find(".sidebar-mainoption-second").slideUp()
      }else{
        $(this).addClass("active");
        $(this).parent().addClass("active")
        $(this).parent().find(".sidebar-mainoption-second").slideDown()
        // Ejecutar funcion de regreso al Home
        events.returnHome()
      }
    },
    // Clic en Sidebar de tipo First
    openMenuFirst: function(){
      if ($(this).hasClass("active")) {
        $(this).parent().next().toggle()
        $(this).toggleClass("rotate")
      } 
      else {
        methods.resetMenuFirst()

        $(this).addClass("active")
        $(this).removeClass("rotate")
        $(this).parent().next().toggle()
      }
    },
    // Clic en Sidebar de tipo Second
    openMenuSecond: function(){
      if ($(this).hasClass("active")) {
        $(this).next().hide()
        $(this).removeClass("active")
      } else {
        $(this).next().show()
        $(this).addClass("active")
      }
    },
    // Regresar a la vista de inicio
    returnHome: function(event){
      $(scopes.allCards).show()
      $(scopes.insights).show()

      // Ocultar todas las secciones hijas
      methods.hideAllSections()

      // Event: Aperturar Patrimonio
      $('#menuPatrimonioTotal .sidebar-mainoption').addClass("active");
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().addClass("active")
      $('#menuPatrimonioTotal .sidebar-mainoption').parent().find(".sidebar-mainoption-second").slideDown()

      // Event: Controlar el scroll principal
      methods.controlMainScroll()

      // Event: Eliminar clases active para todos los elementos: data-section
      $('[data-section*="sec"]').removeClass("active")

     
    },
    // Abrir la sección correspondiente al Parámetro
    openSection: function(){
      // Remover estilo azul para las opciones
      $('[data-section]').removeClass("active")      

      // Ocultar secciones principales
      $(scopes.allCards).hide()
      $(scopes.insights).hide()

      // Ocultar todas las secciones hijas
      methods.hideAllSections()

      // Controlar el scroll principal
      methods.controlMainScroll()

      // En caso de estar en mobile
      $("body").removeClass('inteligo-mobile-open');

      // Capturar la seccion y mostrarla
      var _optMenu = $(this).attr("data-section");
      $('#'+_optMenu).show()

      // Agregar el estilo activo a la opción seleccionada
      $(this).addClass("active")
    }
  };

  var methods = {
    // Controlar el scroll del contenedor principal
    controlMainScroll: function(){
      if (Global.isMobile() == true ){
        $("html, body").animate({ scrollTop: 0 }, 100);
      }else{
        $(scopes.inteligoMain).animate({  scrollTop: 0  }, 100);
      }
    },
    // Ocultar todas las vistas del Proyecto
    hideAllSections: function(){
      $(scopes.creditCard).hide()
      $(scopes.allCorrientes).hide()
      $(scopes.allTrading).hide()
      $(scopes.ctaCorriente).hide()
      $(scopes.ctaTrading).hide()
      $(scopes.detailPage).hide()
      $(scopes.allPrestamos).hide()
      $(scopes.prestamos).hide()
    },
    // Corriente: Resetear estados "active" del Sidebar
    resetSidebarCorriente: function(){
      // Configurar los textos
      $('#secAllCorrientes').find(".cardbox-back-link").attr("data-section", "secCtaCorriente");
      $('#secAllCorrientes').find(".cardbox-back-link span").text("Volver");

      // Menu: Activar la Cta Corriente
      Global.idCorriente = $(this).attr("data-corriente")
      console.log("Id Corriente: "+Global.idCorriente)

      // Menu: Activar Menu Corriente
      methods.menuCorrientes(Global.idCorriente)
    },
    // Trading: Resetear estados "active" del Sidebar
    resetSidebarTrading: function(){
      Global.idTrading = $(this).attr("data-trading")
      console.log("Id Trading: " + Global.idTrading)

      // Menu: Activar Trading
      methods.menuTrading(Global.idTrading)      
    },
    // Resetear estilos "Active" de todo el Sidebar
    resetMenuFirst: function(){
      $('[data-menu="first"]').parent().next().hide()
      $('[data-menu="first"]').removeClass("active rotate")
    },
    // Agregar estilo active dentro del Menú de Préstamos
    configPrestamos: function(){
      setTimeout(function(){
        // Agregar active al Menu
        $('#menuPrestamos').addClass("active")      
      }, 50)
    },
    // Configuración de textos en Corrientes
    configNavCorrienteInd: function(event){
      // Verifivar Corriente Independiente
      Global.CorrienteInd = true;

      // ID del la Cuenta Corriente
      console.log("Corriente Individual: "+$(this).attr("data-menu-corriente-ind"))

      // Configurar los textos
      $('#secAllCorrientes').find(".cardbox-back-link").attr("data-section", "home");
      $('#secAllCorrientes').find(".cardbox-back-link span").text("Volver a mi zona segura");

      // Resetear todos los actives
      methods.resetMenuFirst()
    },
    // Menu: Volver desde Zon Segura en Lista de Corrientes Independientes
    menuPortafolio: function(_idPortfolio){
      setTimeout(function(){
        $("[data-portafolio='"+_idPortfolio+"'").addClass("active")
        $("[data-portafolio='"+_idPortfolio+"'").closest(".block-profile-portfolios-main").find('[data-menu="first"]').addClass("active")
        $("[data-portafolio='"+_idPortfolio+"'").closest(".block-profile-portfolios-main").find('.block-profile-portfolios-secondary').show().addClass("active")
        // $("[data-portafolio='"+_idPortfolio+"'").closest(".block-profile-portfolios-main").find('[data-menu="second"]').addClass("active")
        // $("[data-portafolio='"+_idPortfolio+"'").closest(".portfolio-active-subitems").show()
        // $("[data-portafolio='"+_idPortfolio+"'").closest(".portfolio-active-subitems").find('[data-menu="second"]').show()
      },50)
    },
     // Menu: Cta Corriente ( activar en el menú )
    menuCorrientes: function(_idCorriente){

      setTimeout(function(){
        $("[data-menu-corriente='"+_idCorriente+"'").addClass("active")
        $("[data-menu-corriente='"+_idCorriente+"'").closest(".block-profile-portfolios-main").find('[data-menu="first"]').addClass("active")
        $("[data-menu-corriente='"+_idCorriente+"'").closest(".block-profile-portfolios-main").find('[data-menu="second"]').addClass("active")
        $("[data-menu-corriente='"+_idCorriente+"'").closest(".portfolio-active-subitems").show()
        $("[data-menu-corriente='"+_idCorriente+"'").closest(".portfolio-active-subitems").find('[data-menu="second"]').show()
      },50)
    },
    // Menu: Trading ( activar en el menú )
    menuTrading : function(_idTrading){

      setTimeout(function(){
        $("[data-menu-trading='"+_idTrading+"'").addClass("active")
        $("[data-menu-trading='"+_idTrading+"'").closest(".block-profile-portfolios-main").find('[data-menu="first"]').addClass("active")
        $("[data-menu-trading='"+_idTrading+"'").closest(".block-profile-portfolios-main").find('.block-profile-portfolios-secondary').show()
        $("[data-menu-trading='"+_idTrading+"'").closest(".portfolio-active-subitems").show()
        $("[data-menu-trading='"+_idTrading+"'").closest(".portfolio-active-subitems").find('[data-menu="second"]').addClass("active")
      },50)
    }
  }
    
  var initialize = function () {
    subsEvents();
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    sidebar.init();
  },
  false
);


