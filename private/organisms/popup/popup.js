/*
 * PopUp Inicio - Desktop/mobile
 * @author JCarlosR
 */

var popup = (function () {
  var scopes = {
   
  };
  var subsEvents = function(){  
    //Popup Desktop
    $('#NoShowMoreD').on("click", events.closePopup)
    $('#agreePopupD').on("click", events.closePopup)
    //Popup Mobile
    $('.first-page-intro-top').on("click", events.closePopupMobile)
    $('.discover-trigger-button').on("click", events.firstStepComplete)
    $('#backTo1').on("click", events.backToFirst)
    $('#goTo3').on("click", events.secondStepComplete)
    $('#backTo2').on("click", events.backToSecond)
    $('#goTo4').on("click", events.thirdStepComplete)
    $('#NoShowMoreM').on("click", events.closePopupMobile)
    $('#agreePopupM').on("click", events.closePopupMobile)
  };

  var events = {
    controlModal : function(){
      // Este dato puede venir de un servicio
      // True : mostrar popup  |  False: ocultar popup
      var viewModal = false;
      
      // Se ejecuta el Popup, si "viewModal" = true
      if (!viewModal) {
        $('#inteligoPopup').remove();
        $('#inteligoPopupMobile').remove();
      }else{
        methods.popupInitialize();
        if (Global.isMobile()) {
          methods.popupMobileInitialize();          
        }
      }
    },
    // Cerrar popup de la Intro
    closePopup: function () {
      $('#inteligoPopup').removeClass("open")
      $('#inteligoPopupMobile').removeClass("open")
      $('#layoutBackdrop').removeClass("open")
    },
    // Cerrar popup de la Intro mobile
    closePopupMobile: function () {
      $('.first-page-intro-top').closest('.inteligo-popup-mobile').css("display", "none");
      $('#layoutBackdrop').removeClass("open")
      $('#inteligoPopupMobile').hide()
      $("html, body").animate({
        scrollTop: 0
      }, 100);
    },
    firstStepComplete: function () {
      $('.discover-trigger-button').closest('#introStep1').hide()
      $('#introStep2').show(0)
      $('.dot-step-1.active').removeClass('active')
      $('.dot-step-2').addClass('active')
    },
    backToFirst : function () {
      $(this).closest('#introStep2').hide()
      $('#introStep1').show(0)
      $('.dot-step-2.active').removeClass('active')
      $('.dot-step-1').addClass('active')
    },
    secondStepComplete: function () {
      $(this).closest('#introStep2').hide()
      $('#introStep3').show(0)
      $('.dot-step-2.active').removeClass('active')
      $('.dot-step-3').addClass('active')
    },
    backToSecond: function () {
      $(this).closest('#introStep3').hide()
      $('#introStep2').show(0)
      $('.dot-step-3.active').removeClass('active')
      $('.dot-step-2').addClass('active')
    },
    thirdStepComplete: function () {
      $(this).closest('#introStep3').hide()
      $('#introStep4').show(0)
      $('.dot-step-3.active').removeClass('active')
      $('.dot-step-4').addClass('active')
    },
    backToThird: function () {
      $(this).closest('#introStep4').hide()
      $('#introStep3').show(0)
      $('.dot-step-4.active').removeClass('active')
      $('.dot-step-3').addClass('active')
    }
  };

  var methods = {
    popupInitialize : function(){
      // Mostrar el fondo backdrop
      $('#layoutBackdrop').addClass("open")
      setTimeout(function(){
        $('.inteligo-popup-content').css("opacity", "1")
        $('.tour-intro-popup').css("opacity", "1")
      }, 200)
    },

    popupMobileInitialize : function() {
       // Mostrar el fondo backdrop
       $('#layoutBackdrop').addClass("open")
    }
  }
    

  var initialize = function () {
    events.controlModal()
    subsEvents();
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    popup.init();
  },
  false
);

