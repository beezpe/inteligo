/*
 * Month Picker - Desktop/mobile
 * @author JCarlosR
 */

//FUNCIONES PARA AMPLIAR EL CALENDARIO PERSONALIZADO V. DESKTOP
let today = new Date();
let maxDate = new Date(today.getFullYear(), today.getMonth(), 1);

var currentPicker1;

$(document).click(function () {
  var obj = $('#monthpicker1, #monthpicker2, #monthpicker3, #monthpicker4');
  if (!obj.is(event.target) && !obj.has(event.target).length) {
    $('.inteligo-main').css('overflow', 'auto');
  } else {
    $('.inteligo-main').css('overflow', 'hidden');
  }
});

$(document).ready(function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom left' : (windowHeight >= 600 && windowHeight <= 900) ? 'top left' : 'bottom left';
	console.log('Alto del navegador:', window.innerHeight);
if (currentPicker1) {
    currentPicker1.destroy();
}

	
	currentPicker1 = new AirDatepicker('#monthpicker1', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});

$(window).on('resize', function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom left' : (windowHeight >= 600 && windowHeight <= 900) ? 'bottom left' : 'bottom right';

	if (currentPicker1) {
		currentPicker1.destroy();
	}
	
	currentPicker1 = new AirDatepicker('#monthpicker1', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});


var currentPicker2;

$(document).ready(function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom right' : (windowHeight >= 600 && windowHeight <= 900) ? 'top right' : 'bottom right';

	if (currentPicker2) {
		currentPicker2.destroy();
	}
	
	currentPicker2 = new AirDatepicker('#monthpicker2', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});

$(window).on('resize', function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom left' : (windowHeight >= 600 && windowHeight <= 900) ? 'bottom left' : 'bottom right';

	if (currentPicker2) {
		currentPicker2.destroy();
	}
	
	currentPicker1 = new AirDatepicker('#monthpicker2', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});

/* ============================ CALENDARIOS DE CUENTA TRADING =================================*/

var tradingPicker1;

$(document).ready(function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom left' : (windowHeight >= 600 && windowHeight <= 900) ? 'top left' : 'bottom left';

	if (tradingPicker1) {
		tradingPicker1.destroy();
	}
	
	tradingPicker1 = new AirDatepicker('#mustacheTrading #monthpicker3', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});

$(window).on('resize', function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom left' : (windowHeight >= 600 && windowHeight <= 900) ? 'bottom left' : 'bottom right';

	if (tradingPicker1) {
		tradingPicker1.destroy();
	}
	
	tradingPicker1 = new AirDatepicker('#monthpicker3', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});

var tradingPicker2;

$(document).ready(function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom right' : (windowHeight >= 600 && windowHeight <= 900) ? 'top right' : 'bottom right';

	if (tradingPicker2) {
		tradingPicker2.destroy();
	}
	
	tradingPicker2 = new AirDatepicker('#monthpicker4', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});

$(window).on('resize', function () {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var isMobile = windowWidth <= 425;

	var position = isMobile ? 'bottom left' : (windowHeight >= 600 && windowHeight <= 900) ? 'bottom left' : 'bottom right';

	if (tradingPicker2) {
		tradingPicker2.destroy();
	}
	
	currentPicker1 = new AirDatepicker('#monthpicker4', {
		view: 'months',
		minView: 'months',
		maxDate: maxDate,
		dateFormat: 'MMMM/yyyy',
		isMobile: isMobile,
		autoClose: true,
		position: position,
		locale: {
			months: ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
			dateFormat: 'dd/MM/yyyy',
			firstDay: 1,
		},
	});
});
