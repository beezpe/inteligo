// FUNCIÓN QUE SUMA EL VALOR DE LOS PORTAFOLIOS ASOCIADOS A UN PORTAFOLIO

$('.select-button').click(function() {
  let sum = 0;
  $('.select-button:checked').each(function() {
    sum += parseFloat($(this).closest('.corriente-detail-right').find('.detail-right-bottom_corriente').text().replace(/[^\d.-]/g, ''));
  });
  if (sum > 0) {
    // Agregar separador de miles y millones
    sum = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    // Redondear y mostrar 2 decimales o mostrar 00 precedido de un punto
    if (sum.indexOf('.') !== -1) {
      sum = parseFloat(sum).toFixed(2);
    } else {
      sum = sum + '.00';
    }
    // Mostrar la suma en el elemento con clase "saldo-corriente-amount"
    $('.saldo-corriente-amount').text('USD ' + sum);
  } else {
    // Mostrar 0.00 en el elemento con clase "saldo-corriente-amount"
    $('.saldo-corriente-amount').text('USD 0.00');
  }
});


$(document).ready(function () {
  $(".left-button").on("click", function () {
    $(".left-button").addClass("active");
    $('.corrientes-cards-title').css('color', '#233E99');
    $('.corrientes-cards-subtitle').text(function (_, txt) {
      return txt.replace('EUR', 'USD')
    });
    $(".right-button").removeClass("active");
  });

  $(".right-button").on("click", function () {
    $(".right-button").addClass("active");
    $('.corrientes-cards-title').css('color', '#126B62');
    $('.corrientes-cards-subtitle').text(function (_, txt) {
      return txt.replace('USD', 'EUR')
    });
    $(".left-button").removeClass("active");
  });
});


/*
 * Corriente
 * @author José, Diego, Mfyance
 */

var cta_corriente = (function () {
  var scopes = {
    urlCorriente    : "_components/cta-corriente/mocks/corriente.json"
  }

  var services = {
    loadPromises : function() {
      // CARGA LOS DETALLES DEL MOCK DE CUENTA CORRIENTE
      Global.promiseCorriente = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlCorriente,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });

      Global.promiseCorriente.then(function(){
        methods.loadTable() // CARGO LOS DETALLES DE CTA. CORRIENTE
      })
    },
  }

  var subsEvents = function(){ 
    // Event: Mostrar Calendario personalizado
    $('#filterCorriente').on("change",events.openCustomizeCalendar)
    
    // Event: Ver detalle de cada Préstamo
    $('[data-corriente]').on("click", events.showDetailCorriente)
    $('[data-menu-corriente]').on("click", events.showDetailCorriente)

    // Event: Ver detalle de cada Cta Corriente
    $('[data-modal="email"]').on("click", events.sendEmailCorriente)

    // Event: Resetear el calendario al valor por defecto
    $('body').on("click", '[data-section="secCtaCorriente"]', function(){
      Global.reloadSelect('#filterCorriente', 1)
    });
  };
  
  var events = {
    // Obtener el ID de la Cta Corriente donde hice clic
    showDetailCorriente: function(){
      // Aqui se debe ejcutar servicios para traer data por ID Préstamos
      var idCorriente = Global.idCorriente
    },
    openCustomizeCalendar: function(){
      if($(this).val() == 4){
        $('#corrienteCalendar').show()
      }else{
        $('#corrienteCalendar').hide()
      }
		},
    sendEmailCorriente: function(){
      var _id = Global.idCorriente;
      if(_id != undefined){
        var _text = "Información sobre Cuenta corriente"
        var _date = moment().format('DD MMM YYYY');
        Global.modalAsuntoShow(_text+" "+ _id + " al "+_date)
      }
    }
  };

  var methods = {

    loadTable: function() {
      Global.promiseCorriente.then(function(_oData){

        // Aquí ejecutamos el Mustache para tabla Desktop
        var template99 = document.getElementById('tmpCorrienteTable').innerHTML;
        var rendered99 = Mustache.render(template99, _oData );
        $('#mustacheCorrienteTable').empty().html(rendered99);

        // Aquí ejecutamos el Mustache para tabla Mobile
        var template98 = document.getElementById('tmpCorrienteTableMobile').innerHTML;
        var rendered98 = Mustache.render(template98, _oData );
        $('#mustacheCorrienteMobile').empty().html(rendered98);
      });
    }
  }

  var initialize = function () {
    subsEvents();
    services.loadPromises()
    
    // Ejecutar calendario
    Global.runSelect("#filterCorriente")

    // Ejecutar fecha ida/regreso
    //       $('#test').datepicker({
    //         language: 'es',
    //         format: 'mm-yyyy',
    //         startDate: '0d',
    //         format: "mm-yyyy",
    // startView: "months", 
    // minViewMode: "months"
    //       });

  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    cta_corriente.init();
  },
  false
);