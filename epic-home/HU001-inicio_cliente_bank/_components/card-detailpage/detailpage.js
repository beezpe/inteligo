var card_detailpage = (function () {
  var scopes = {

    // urlDetailPage    : "https://demo6027168.mockable.io/insights"
    urlDetailPage    : "_components/card-detailpage/mocks/detailpage.json"
  }

  var services = {
    loadPromises : function() {
      // Cargar el DetailPage con data según el ID de analisis
      Global.promiseDetailPage = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlDetailPage,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {            
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });  


      // Cargar las cajas del detailPage, con la misma data del Card3
      Global.promiseAnalisis.then(function(_oData){
        var tmpDetailPageHead = document.getElementById('tmpDetailPageHead').innerHTML;
        var renderDetailPageTable = Mustache.render(tmpDetailPageHead, _oData );
        $('#mustacheDetailPageHead').empty().html(renderDetailPageTable).promise().done(function () {
          // Levantar modal de Saber Más
          services.loadModalSaberMas()
          // Levantar modal de "Signica esto"
          services.loadDataSignificaEsto()
        })

      })

      // Crear una variable temporal de la data de DetailPage
      Global.promiseDetailPage.then(function(_oData){        
        Global.dataMockDetailPage = _oData;
        methods.controlModals(_oData)
      })

      // El valor debe ser "true" : Existe insights, "false" : No existe insigths
      Global.isInsights = true;
    },
    // Crear modal para Botón "Qué significa esto"
    loadDataSignificaEsto : function(){
      var tmpSignifica = {
        "id": "detailpage-distribucion",
        "modalContent" : {                  
            "title": "Distribución por subtipo de activo",
            "subtitle": "¿Qué clase de activos existen?",
            "icon" : "icon-modal-pc",
            "desc" : "<p>Entre las principales clases de activos encontramos:</p><ul>   <li><p><b>Activos de renta fija:</b> Son valores mobiliarios (bonos, certificados de depósito, entre otros) que representan una deuda emitida por empresas, gobiernos u organismos supranacionales, por las que el inversionista recibe un pago fijo periódico o a vencimiento.</p></li><li><p><b>Activos de renta variable:</b> Conformados por acciones, bonos convertibles, ADRs, ETFs, entre otros, en los cuales los pagos futuros dependen del resultado de la actividad económica que realiza el emisor del activo, por lo que no se conocen los flujos futuros con certeza.</p></li><li><p><b>Activos de renta alternativa:</b> Comprenden a los commodities (oro, plata, cobre, soya, petróleo, entre otros), a los fondos de capital Privado, a los bienes raíces, entre otros.</p></li></ul>",
            "button1": "Siguiente",
            "button1link": "next",
            "button2": "Entendido",
            "button2link": "close"
        },
        "modalContent3" : { 
          "title": "Distribución por subtipo de activo",
          "subtitle": "¿Qué es un subtipo de activo?",
          "icon" : "icon-modal-pc",
          "desc" : "<p>Un subtipo de activo es una parte de una amplia clase de activos que se desglosa para poder brindarle un mayor detalle de los activos dentro del subtipo. Estos comparten características similares entre sí y también con la clase de activos más amplia de la que forman parte.</p><p>Tener este desgloce por subtipo de activos es importante para poder construir una cartera diversificada.</p>",
          "button1": "Atrás",
          "button1link": "prev",
          "button2": "Entendido",
          "button2link": "close"
        }
      }
      Global.modalInitialize(2,tmpSignifica);  
    },
    // Crear modal para Botón "Quiero saber más"
    loadModalSaberMas : function(){
      var tmpSabermas = {
        "id": "detailpage-sabermas",        
        "modalContent": {
          "title": "Importancia del portafolio modelo",
          "desc": "<p>Un portafolio es un conjunto de activos financieros. El portafolio modelo (o benchmark) es un portafolio en el que la combinación de los activos financieros seleccionados se realiza en base al perfil de riesgo y horizonte de inversión (entre otros aspectos) de cada tipo de inversionista, buscando optimizar la relación entre riesgo y retorno del portafolio general.</p>",
          "subtitle": "¿Qué es un portafolio modelo?",
          "icon" : "icon-modal-portafolio",
          "button1": "Siguiente",
          "button1link": "next",
          "button2": "Entendido",
          "button2link": "close"
        },
        "modalContent3" : { 
          "title": "Importancia del portafolio modelo",
          "icon" : "icon-modal-portafolio",
          "desc" : "<p>Es importante que su perfil de riesgo esté alineado a un portafolio modelo, ya que éste ha sido diseñado para abordar diversas necesidades.</p>",
          "desc2" : "<ul><li><p><b>Diversificación:</b> Para que se eviten pérdidas significativas ante escenarios negativos en un mercado particular.</p></li><li><p><b>Análisis profesional:</b> Permiten evaluar las decisiones del gestor del portafolio midiendo los resultados que obtiene en comparación al portafolio modelo, creando así estrategias de inversión para usted.</p></li></ul>",
          "button1": "Regresar al anterior",
          "button1link": "prev",
          "button2": "Entendido",
          "button2link": "close"
        }
      }
      
      Global.modalInitialize(2,tmpSabermas);  
    }
  }

  var subsEvents = function(){  
    $('[data-backhome="detailpage"]').on("click", events.hideDetailPage)
    $('#mustacheAnalisisTable').on("click", ".analisis-detail", events.showDetailpage)
    $('#mustacheAnalisisTableMobile').on("click", ".analisis-mtable-box", events.showDetailpage)
    $('#mustacheDetailPageHead').on("click", ".detailpage-activos-box", events.changeDetailPage)
    $('#mustacheDetailPageInfo').on("click", ".analisis-mtable-sabermas-link", events.openVermasTable)
    $('#mustacheDetailPageHead').on("click", "*[data-detailpage]", events.openModalDetailPage)
    $('#mustacheDetailPageInfo').on("click", "*[data-detailpage]", events.openModalDetailPage)
    $('#mustacheDetailPageInfo').on("click", "#btnContactarAsesor", function(){
      // Abri modal de contactar
      Global.modalOpen("#modal-inteligo-contact")
    })
    // Abrir modal de ¿Que significa eso?
    $('#mustacheDetailPageInfo').on("click", "#btnModalDistribucion", function(){
      Global.modalOpen("#modal-detailpage-distribucion")
    })
    // Abrir la modal de ayuda
    $("#btnDetailPageSaberMas").on("click", function () {
      Global.modalOpen("#modal-detailpage-sabermas")
    });
  };
  
  var events = {

    // Abrir modales dentro del DetailPage
    openModalDetailPage: function(){
      var idModal = $(this).attr("data-detailpage");
      Global.modalOpen("#"+idModal)
    },
    // Abrir "ver mas" dentro de card de subtipos en Mobile
    openVermasTable: function () {
      var txt1 = "Saber más";
      var txt2 = "Ver menos";

      if($(this).hasClass("active")){
        $(this).removeClass("active")
        $(this).find(".analisis-mtable-sabermas-txt").text(txt1)
      }else{
        $(this).addClass("active")
        $(this).find(".analisis-mtable-sabermas-txt").text(txt2)
      }
    },
    // Retornar a la vista principal de la paltaforma
    hideDetailPage: function(){
      $('#secInsights').show()
      $('#secMainCards').show()
      $('#secDetailpage').hide()

      // Debemos tener el valor DE LA EXISTENCIA DE INSIGHTSH
      // Para verificar que comportamiento optar
      // True: Existe insigths , False: No existe insigths
      if(Global.isInsights){
        
        // Controlar el scroll cuando este regresando
        if (Global.isMobile() == true ){
          var idSection = parseInt($("#mustacheAnalisisTableMobile").offset().top) - 340
          $("html, body").animate({
            scrollTop: idSection
          }, 100);
        }
        else{
          var idSection = parseInt($("#cardAnalisis").position().top) - 45
          $("#inteligoMain").animate({
            scrollTop: idSection
          }, 100);
        }

      } else{

        // Controlar el scroll cuando este regresando
        if (Global.isMobile() == true ){
          var idSection = parseInt($('#mustacheAnalisisTableMobile').offset().top) - 240
          $("html, body").animate({
            scrollTop: idSection
          }, 100);
        }
        else{
          var idSection = parseInt($("#cardAnalisis").position().top) - 70
          $("#inteligoMain").animate({
            scrollTop: idSection
          }, 100);
        }

      }
      
    },
    // Abrir Detailpage de cada clase activo
    showDetailpage: function(){     

       // ID que identifica el parametro enviado al dar clic
      Global.idDetailPage = $(this).attr("data-id")      

      // Controlar el scroll cuando este regresando
      if (Global.isMobile() == true ){
        $("html, body").animate({
          scrollTop: 0
        }, 100);
      }else{
        $("#inteligoMain").animate({
          scrollTop: 0
        }, 100);
      }

      // Obtener data filtrada 
      var _oDataDetailPage = methods.getDataMockDetailPage(Global.idDetailPage)
      // Enviar data a la plantilla Mustache
      methods.loadDetailPage(_oDataDetailPage[0], Global.idDetailPage);

      // Agregar ID para link que abre modales de asignaciÃ³n
      $('#modalAsignacion').attr('data-detailpage', 'modal-asignacion-'+Global.idDetailPage+'')

    },
    // Interacción en las cajas de activos
    changeDetailPage: function(){
      var id = $(this).attr("data-id")
      Global.idDetailPage = id;

      // Agregar ID para link que abre modales de asignación
      $('#modalAsignacion').attr('data-detailpage', 'modal-asignacion-'+Global.idDetailPage+'')

      // Obtener data filtrada 
      var _oDataDetailPage = methods.getDataMockDetailPage(Global.idDetailPage)      
      // Enviar data a la plantilla Mustache
      methods.loadDetailPage(_oDataDetailPage[0], Global.idDetailPage);
    }
  };

  var methods = {
    // Generar modales de manera dinámica
    controlModals: function(oDataModals){
      
      for(var i=0; i < oDataModals.datos.detailpage.length ; i++){

        for(var j=0; j < oDataModals.datos.detailpage[i].modales.length ; j++){

          // Generar modales, enviando su ID y todo el Objeto
          Global.modalInitialize(
            oDataModals.datos.detailpage[i].modales[j].template, 
            oDataModals.datos.detailpage[i].modales[j]
          )
        }        
      }
    },

    // Obtener data temporal, filtrada por el "idanalisis"
    getDataMockDetailPage : function(id){
      var _oDataDetailPage =  Global.dataMockDetailPage.datos.detailpage.filter(function(oData) {
        return oData.idanalisis == id;
      });
      return _oDataDetailPage;
    },
    // Cargar la data principal de DetailPage
    loadDetailPage: function(_oData, _id) { 
      // Agregar activo a las cajas, segun el ID del detailpage
      $('#mustacheDetailPageHead .detailpage-activos-box').removeClass("active")
      $('#mustacheDetailPageHead .detailpage-activos-box[data-id="'+_id+'"]').addClass("active")

      // Renderizar datos de detailpage
      var tmpDetailPageInfo = document.getElementById('tmpDetailPageInfo').innerHTML;
      var renderDetailPageInfo = Mustache.render(tmpDetailPageInfo, _oData );
      $('#mustacheDetailPageInfo').empty().html(renderDetailPageInfo);
    }
  }

  var initialize = function () {
    subsEvents();
    services.loadPromises()

  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    card_detailpage.init();
  },
  false
);