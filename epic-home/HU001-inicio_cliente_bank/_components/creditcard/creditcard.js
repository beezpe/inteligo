var creditcard = (function () {
	var scopes = {
		// urlCreditcard    : "https://demo3358914.mockable.io/creditcard"
		urlTCmovimientos: '_components/creditcard/mocks/movimientos.json',
		urlCreditCard: '_components/creditcard/mocks/tarjetas.json',
		urlAvisoViaje: '_components/creditcard/mocks/viajes.json',
		urlPaises: '_components/creditcard/mocks/paises.json',
		txtShowMovs: 'Ver todos los movimientos',
		txtHideMovs: 'Ocultar movimientos',
		maxMovs: 6,
	};

	var services = {
		loadPromises: function () {
			// Llamamos Servicio a Info general de la TC
			Global.promiseCreditCard = new Promise(function (resolve, reject) {
				$.ajax({
					url: scopes.urlCreditCard,
					contentType: 'application/json; charset=utf-8',
					cache: false,
					dataType: 'json',
					type: 'POST',
					async: true,
					crossDomain: true,
					headers: {
						accept: 'application/json',
						'Access-Control-Allow-Origin': '*',
					},
					success: function (data) {
						// Objeto de tarjetas
						Global.oCreditCards = data.datos.cuenta.tarjetas;
						// Agregar una clase cuando se renderizan en pantallas grandes
						if (Global.oCreditCards.length == 4) {
							if (Global.isWidth > 1600) {
								$('#mustacheCreditCard').parent().addClass('creditcard-container-big');
							} else {
								$('#mustacheCreditCard').parent().removeClass('creditcard-container-big');
							}
						}

						resolve(data);
					},
					error: function (error) {
						reject(error);
					},
				});
			});

			// Cargamos tods las tarjetas
			Global.promiseCreditCard.then(function () {
				methods.loadAllCreditCards();
			});

			// Llamamos servicio para cargar modal de viaje
			Global.promiseAvisoViaje = new Promise(function (resolve, reject) {
				$.ajax({
					url: scopes.urlAvisoViaje,
					contentType: 'application/json; charset=utf-8',
					cache: false,
					dataType: 'json',
					type: 'POST',
					async: true,
					crossDomain: true,
					headers: {
						accept: 'application/json',
						'Access-Control-Allow-Origin': '*',
					},
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
					},
				});
			});

			// Cargar todo los paises
			Global.promiseListCountries = new Promise(function (resolve, reject) {
				$.ajax({
					url: scopes.urlPaises,
					contentType: 'application/json; charset=utf-8',
					cache: false,
					dataType: 'json',
					type: 'POST',
					async: true,
					crossDomain: true,
					headers: {
						accept: 'application/json',
						'Access-Control-Allow-Origin': '*',
					},
					success: function (data) {
						// Variable global: Todo los paises
						Global.oPaisesTodos = data;
						// Variable global: Objeto de los paises agregados
						Global.oPaises = [];
						resolve(data);
					},
					error: function (error) {
						reject(error);
					},
				});
			});

			// Generar Modal de Aviso Viaje
			Global.promiseAvisoViaje.then(function (_oData) {
				// oExtra es una función que se ejcutará posterior a la creación de la Modal
				var oExtra = function () {
					// Carga de paises
					var options = {
						url: scopes.urlPaises,
						getValue: 'name',
						list: {
							match: {
								enabled: true,
							},
							onSelectItemEvent: function () {
								var _isPais = $('#listCountries').getSelectedItemData().code;
								$('#listCountries').attr('data-pais', _isPais);

								var isPais = methods.controlRepeatCountries($('#listCountries').attr('data-pais'));
								if (isPais) {
									$('#btnAddCountry').removeAttr('disabled');
								}
							},
						},
						theme: 'square',
					};
					// Ejecutar los paises
					$('#listCountries').easyAutocomplete(options);

					// Elegir país, dándole clic a una de las opciones
					$('#inteligoContentModals').on('click', '.easy-autocomplete-container li', function () {
						var isPais = methods.controlRepeatCountries($('#listCountries').attr('data-pais'));
						if (isPais) {
							$('#btnAddCountry').removeAttr('disabled');
						}
					});

					// Ejecutar fecha ida/regreso
					$('#inpFechaIda, #inpFechaRegreso').datepicker({
						language: 'es',
						format: 'dd-mm-yyyy',
						startDate: '0d',
					});

					// Abrir cuando se da click en su ícono
					$('#inpFechaIda')
						.next()
						.on('click', function () {
							$('#inpFechaIda').datepicker('show');
						});
					$('#inpFechaRegreso')
						.next()
						.on('click', function () {
							$('#inpFechaRegreso').datepicker('show');
						});

					// Detectar el cambio de fechas, tanto de ida y vuelta
					$('#inpFechaIda')
						.datepicker()
						.on('changeDate', function (e) {
							setTimeout(function () {
								$('#inpFechaRegreso').datepicker('destroy');
								$('#inpFechaRegreso').datepicker({
									language: 'es',
									format: 'dd-mm-yyyy',
									startDate: $('#inpFechaIda').val(),
								});
							}, 200);

							// En caso de elegir una fecha que no es lógica
							if (methods.controlDatesSuccess() == false) {
								$('#inpFechaRegreso').val('');
								$('#inpFechaRegreso').datepicker('startDate,"0d"');
								$('#inpFechaRegreso').datepicker('setStartDate', '0d');
								$('#inpFechaRegreso').datepicker('setEndDate', 0);
								$('#inpFechaRegreso').datepicker('update');
							}
						});

					// Validar cuando haya cambio de data
					$('#inpFechaIda, #inpFechaRegreso').on('change', function () {
						// Consultar si hay algun campo faltante en el form
						events.controlFormViaje();
					});

					// Cuando se hace cambio al check, debe validar
					$('#inpSolicitarConstacia').on('change', function () {
						// Consultar si hay algun campo faltante en el form
						events.controlFormViaje();
					});

					// Validar correo
					$('#viajeCorreo').on('keyup keypress', function () {
						// Consultar si hay algun campo faltante en el form
						events.controlFormViaje();
					});

					// Pintamos los selectores con las tarjetas
					methods.generateSelectCreditcard(Global.oCreditCards);

					// Detectar el cambio de valor en Mob Tarjetas
					$('[name=mob-sel-tarjetas').on('change', function () {
						$('#selTarjetas').val($(this).val());
						$('#selTarjetas').niceSelect('update');
						// Consultar si hay algun campo faltante en el form
						events.controlFormViaje();
					});

					// Cerrar sidebarbottom de Tarjetas
					$('#sidebarbottomTC .icon-close').on('click', function () {
						$('#inteligoModalMobile').removeClass('open');
					});
					// Abrir Modal Mobile con las Tarjetas
					$('[data-mobile="openCreditcards"]').on('click', function () {
						$('#inteligoModalMobile').addClass('open');
					});

					// Crear modal inline para Mobile (Ida)
					$('#inpFechaIda2-inline').datepicker({
						inline: true,
						language: 'es',
						format: 'dd-mm-yyyy',
						startDate: moment().format('DD-MM-YYYY'),
					});
					// Crear modal inline para Mobile (Regreso)
					$('#inpFechaRegreso2-inline').datepicker({
						inline: true,
						language: 'es',
						format: 'dd-mm-yyyy',
						startDate: moment().format('DD-MM-YYYY'),
					});

					// Obtener valores del Dtepicker inline (Ida)
					$('#inpFechaIda2-inline').on('changeDate', function () {
						$('#inpFechaIda2').val($('#inpFechaIda2-inline').datepicker('getFormattedDate'));
						$('#inpFechaIda').val($('#inpFechaIda2-inline').datepicker('getFormattedDate'));

						// Refrescar el punto de partida del Regreso
						setTimeout(function () {
							$('#inpFechaRegreso2-inline').datepicker('destroy');
							$('#inpFechaRegreso2-inline').datepicker({
								language: 'es',
								format: 'dd-mm-yyyy',
								startDate: $('#inpFechaIda2').val(),
							});
						}, 200);

						// En caso de elegir una fecha que no es lógica
						if (methods.controlDatesSuccess() == false) {
							$('#inpFechaRegreso, #inpFechaRegreso2').val('');
							$('#inpFechaRegreso, #inpFechaRegreso2').datepicker('startDate,"0d"');
							$('#inpFechaRegreso, #inpFechaRegreso2').datepicker('setStartDate', '0d');
							$('#inpFechaRegreso, #inpFechaRegreso2').datepicker('setEndDate', 0);
							$('#inpFechaRegreso, #inpFechaRegreso2').datepicker('update');
						}

						// Validar en caso esté lleno la IDA, cerrar la modal de CAlendar inline
						if ($('#inpFechaRegreso').val() != '') {
							$('[data-mobile=closeCalendars]').trigger('click');
						} else {
							$('[data-mobile=openCalendarRegreso]').trigger('click');
						}
					});
					// Obtener valores del Dtepicker inline (Vuelta)
					$('#inpFechaRegreso2-inline').on('changeDate', function () {
						$('#inpFechaRegreso2').val($('#inpFechaRegreso2-inline').datepicker('getFormattedDate'));
						$('#inpFechaRegreso').val($('#inpFechaRegreso2-inline').datepicker('getFormattedDate'));
						// Validar en caso esté lleno la IDA, cerrar la modal de CAlendar inline
						if ($('#inpFechaIda').val() != '') {
							$('[data-mobile=closeCalendars]').trigger('click');
						} else {
							$('[data-mobile=openCalendarIda]').trigger('click');
						}
					});

					// Mostrar respectivos calendarios segun el clic en cada input
					$('[data-mobile=openCalendarIda]').on('click', function () {
						$('#inpFechaIda2-inline').show();
						$('#inpFechaRegreso2-inline').hide();
						$('#inpFechaIda2').addClass('active');
						$('#inpFechaRegreso2').removeClass('active');
					});
					$('[data-mobile=openCalendarRegreso]').on('click', function () {
						$('#inpFechaRegreso2-inline').show();
						$('#inpFechaIda2-inline').hide();
						$('#inpFechaRegreso2').addClass('active');
						$('#inpFechaIda2').removeClass('active');
					});

					// Abrir modal con rango de fechas inline (Ida)
					$('[data-mobile=showCalendarIda]').on('click', function () {
						$('.inteligo-modal-calendars').show();
						$('[data-modal="step-2"]').hide();
						$(this).closest('.inteligo-modal').find('.inteligo-modal-close').hide();
						$('[data-mobile=openCalendarIda]').trigger('click');
						$('#modal-avisoviaje').addClass('inteligo-modal-height');
					});
					// Abrir modal con rango de fechas inline (Vuelta)
					$('[data-mobile=showCalendarVuelta]').on('click', function () {
						$('.inteligo-modal-calendars').show();
						$('[data-modal="step-2"]').hide();
						$(this).closest('.inteligo-modal').find('.inteligo-modal-close').hide();
						$('[data-mobile=openCalendarRegreso]').trigger('click');
						$('#modal-avisoviaje').addClass('inteligo-modal-height');
					});
					// Cerrando los calendarios inline
					$('[data-mobile=closeCalendars]').on('click', function () {
						$('.inteligo-modal-calendars').hide();
						$('[data-modal="step-2"]').show();
						$('#modal-avisoviaje').removeClass('inteligo-modal-height');
						$(this).closest('.inteligo-modal').find('.inteligo-modal-close').show();
						// Consultar si hay algun campo faltante en el form
						events.controlFormViaje();
					});

					// Control de validaciones
					$('#selTarjetas').on('change', function () {
						// Consultar si hay algun campo faltante en el form
						setTimeout(function () {
							events.controlFormViaje();
						}, 100);
					});

					// Consultar si hay algun campo faltante en el form
					$('#inpListaPaises, #inpFechaIda, #inpFechaRegreso').on('click', function () {
						setTimeout(function () {
							events.controlFormViaje();
						}, 100);
					});

					// Agregar pais en la seccion de Tags
					$('#btnAddCountry').on('click', function () {
						$('#listCountries').val('');
						$(this).attr('disabled', true);

						var _pais = $('#listCountries').attr('data-pais');

						// Obtener el codigo de pais, segun el filtrado realizado
						var _oDataPais = Global.oPaisesTodos.filter(function (oData) {
							return oData.code == _pais;
						});

						// Agregar tags de los paises en el bloque superior
						var tmpTagPais = `<div class="list-country" data-pais="${_oDataPais[0].code}">
                                ${_oDataPais[0].name} <i class="icon icon-delete"></i>
                              </div>`;
						$(tmpTagPais).appendTo($('#blockCountries'));
						Global.oPaises.push(_oDataPais[0]);
						// Agregar los paises en el input hidden
						$('#inpTotalPaises').val(JSON.stringify(Global.oPaises));

						// Deshabilitamos el boton
						$('#btnAddCountry').attr('disabled', true);

						// Consultar si hay algun campo faltante en el form
						events.controlFormViaje();
					});

					// Eliminar pais de la seccion de Tags
					$('#blockCountries').on('click', '.list-country .icon', function () {
						var _epais = $(this).parent().attr('data-pais');

						// Quitar desabilitado de la lista del Select2
						$('#listPaises')
							.children('option[value=' + _epais + ']')
							.removeAttr('disabled');
						$('#listPaises').trigger('change.select2');

						// Detectar posicion del pais dentro del array
						var i, pos;
						for (i in Global.oPaises) {
							if (Global.oPaises[i].code === _epais) {
								pos = i;
								break;
							} else {
								pos = -1;
							}
						}
						if (pos > -1) {
							Global.oPaises.splice(pos, 1);
						}

						// Eliminar el tag de la seccion superior
						$(this).fadeOut('fast', function () {
							$(this).parent().remove();
						});

						// Verificar si queda vacio el objeto, para deshabilitar el boton de "Añadir"
						if (Global.oPaises.length == 0) {
							$('#btnAddCountry').attr('disabled', true);
						}

						// Actualizar los paises en el input hidden
						if (Global.oPaises.length == 0) {
							$('#inpTotalPaises').val('');
						} else {
							$('#inpTotalPaises').val(JSON.stringify(Global.oPaises));
						}

						// Consultar si hay algun campo faltante en el form
						events.controlFormViaje();
					});

					// Detectar cambio de valor en lista de paises
					$('#listPaises').on('change', function () {
						var _pais = $('#listPaises').val();
						if (_pais !== null) {
							$('#btnAddCountry').removeAttr('disabled');
						} else {
							$('#btnAddCountry').attr('disabled', true);
						}
					});

					// Eliminar datos al cerrar la modal
					$('#inteligoContentModals').on('click', "[data-modal='close']", function () {
						// Eliminar los tags ingresados
						$('#blockCountries').html('');
						// Borrar los tags seleccionados
						Global.oPaises = [];

						// Quitamos attr deshabilitados en el select2
						$('#listPaises').children('option').removeAttr('disabled');
						// Resetear selector
						$('#listPaises').select2('val', '-1');
						// Refrescamos el Select2
						$('#listPaises').trigger('change.select2');

						// Resetear checkbox de "Solicitar constancia"
						if ($('#inpSolicitarConstacia').is(':checked') == true) {
							$('#inpSolicitarConstacia').removeAttr(':checked');
							$('#inpSolicitarConstacia').trigger('click');
						}

						// Resetear valores de TC en mobile
						$('.inteligo-sidebarbottom-tc-option:first-child .container-radio').trigger('click');

						// Resetear valores de tarjeta
						$('#selTarjetas').val('1');
						$('#selTarjetas').niceSelect('update');

						// Resetear valores del calendario
						$('#inpFechaIda, #inpFechaRegreso, #inpFechaIda2-inline, #inpFechaIda1-inline').datepicker(
							'startDate,"0d"',
						);
						$('#inpFechaIda, #inpFechaRegreso, #inpFechaIda2-inline, #inpFechaIda1-inline').datepicker(
							'setStartDate',
							'0d',
						);
						$('#inpFechaIda, #inpFechaRegreso, #inpFechaIda2-inline, #inpFechaIda1-inline').datepicker('setEndDate', 0);
						$('#inpFechaIda, #inpFechaRegreso, #inpFechaIda2-inline, #inpFechaIda1-inline').datepicker('update');
					});

					// Checkbox cuando desea solicitar constancia, asi agregar correo
					$('#inteligoContentModals').on('change', '#inpSolicitarConstacia', function () {
						var isValue = $('#inpSolicitarConstacia').is(':checked');
						$('#viajeCorreo').val('');
						if (isValue) {
							$('#contentEmail').show();
						} else {
							$('#contentEmail').hide();
						}
					});

					// Casuisticas de dimension de Modal, segun clic :"Next, Prev, Close"
					// $('#inteligoContentModals').on('click', "[data-modal='next']", function () {
					// 	$(this).closest('.inteligo-modal').addClass('inteligo-modal-top');
					// });
					// $('#inteligoContentModals').on(
					// 	'click',
					// 	"[data-modal='prev'], [data-modal='close'], [data-modal='send']",
					// 	function () {
					// 		$(this).closest('.inteligo-modal').removeClass('inteligo-modal-top');
					// 	},
					// );
				};

				Global.modalInitialize(7, _oData, oExtra);
			});
		},
	};

	var subsEvents = function () {
		$('#inteligoContentModals').on('keypress keyup', '#listCountries', events.controlCountry);
		$('#mustacheCreditCard').on('click', '.creditcard-container-card', events.controlCardView);
		$('#mustacheCreditCard').on('click', '#btnValidFormViaje', events.controlFormViaje);
		$('#btnShowMore').on('click', events.controlMovimientos);
		$('#btnModalViaje').on('click', function () {
			// Abrir modal de Aviso de viaje
			Global.modalOpen('#modal-avisoviaje');
		});

		$('#secCreditCard').on('click', '[data-modal="email"]', events.sendEmailPrestamos);

		// Redimensionar el dispositivo
		window.addEventListener('resize', function (event) {
			// Variables para la cantidad de tarjetas
			if (Global.oCreditCards.length == 4) {
				if (Global.isWidth > 1600) {
					$('#mustacheCreditCard').parent().addClass('creditcard-container-big');
				} else {
					$('#mustacheCreditCard').parent().removeClass('creditcard-container-big');
				}
			}
		});
	};

	var events = {
		// Evento para enviar Email con Asunto
		sendEmailPrestamos: function () {
			var _id = Global.idCreditCard;
			if (_id != undefined) {
				var _text = 'Información sobre su tarjeta de crédito';
				var _date = moment().format('DD MMM YYYY');
				var readCardSelected = Global.oCreditCards.find(function (card) {
					return card.idCard === _id;
				});
				Global.modalAsuntoShow(_text + ' ' + readCardSelected.cardNumber + ' al ' + _date);
			}
		},
		// Controlar si esta vacío el espacio de Pais
		controlCountry: function () {
			var _valPais = $(this).val();
			if (_valPais == '') {
				$('#btnAddCountry').attr('disabled', true);
			}
		},
		// Mostrar y ocultar movimientos
		controlMovimientos: function () {
			if ($(this).text() === scopes.txtShowMovs) {
				$(this).text(scopes.txtHideMovs);
				$('#mustacheCreditcardMovimientos').find('.creditcard-table-tr').removeAttr('style');
			} else {
				$(this).text(scopes.txtShowMovs);
				$('#mustacheCreditcardMovimientos .creditcard-table-tr').slice(scopes.maxMovs).hide();
			}
		},
		// Controlar viaje return: true || false
		controlFormViaje: function () {			
			setTimeout(function(){
				if (events.validFormViaje() == true) {				
					$('#btnValidFormViaje').removeAttr('disabled');
				} else {					
					$('#btnValidFormViaje').attr('disabled', true);
				}
			},100)
		},
		// Validar formulario de todos los campos
		validFormViaje: function () {
			var statusFormViaje 	= '';
			var _inpListaPaises     = $('#inpTotalPaises').val();
			var _inpFechaIda 		= $('#inpFechaIda').val();
			var _inpFechaRegreso 	= $('#inpFechaRegreso').val();
			var _selTarjetas 		= $('#selTarjetas').val();
			var _inpFechaIda 		= $('#inpFechaIda').val();
			var _viajeCorreo 		= $('#viajeCorreo').val();

			// Comprobar cada campo
			if (
				_selTarjetas != '' &&
				_inpFechaIda != '' &&
				_inpFechaIda != '' &&
				_inpFechaRegreso != '' &&
				_inpListaPaises != ''
			) {
				if ($('#inpSolicitarConstacia').is(':checked') == true) {
					if (Global.isMail(_viajeCorreo) == true) {
						statusFormViaje = true;
					} else {
						statusFormViaje = false;
					}
				} else {
					statusFormViaje = true;
				}
			} else {
				statusFormViaje = false;
			}

			return statusFormViaje;
		},
		// Mostar la data de la tarjeta al inicio
		controlCardView: function () {
			// Detectar que no este activo para ejecutar consultas
			if (!$(this).hasClass('active')) {
				// Estado activo de tarjetas
				$('.creditcard-container-card').removeClass('active');
				$(this).addClass('active');

				// ID de la TC | Detectar id de donde se hizo clic
				Global.idCreditCard = $(this).attr('id');

				// Control change data about Credit Cards
				methods.controlDataCards(Global.idCreditCard);

				// Controlar scroll para las tarjetas
				methods.controlSliderCards($(this));
			}
		},
	};

	var methods = {
		// Controlar Fechas Maximas
		controlDatesSuccess: function () {
			var _status;
			var _ida = $('#inpFechaIda').val();
			var _vuelta = $('#inpFechaRegreso').val();

			// Corroborar cuando ambos NO esten vacios
			if (_ida != '' && _vuelta != '') {
				var _startTime = moment(_ida, 'DD-MM-YYYY').format('YYYY-MM-DD');
				var _endTime = moment(_vuelta, 'DD-MM-YYYY').format('YYYY-MM-DD');

				var _duration = moment.duration(moment(_endTime).diff(_startTime));
				var _hours = _duration.asDays();

				if (_hours > 0) {
					_status = true;
				} else {
					_status = false;
				}
			} else {
				_status = false;
			}
			return _status;
		},
		// Controlar Pais si está repetido en el Objeto
		controlRepeatCountries: function (_pais) {
			var result;
			var _isRepeat = Global.oPaises.filter(function (oData) {
				return oData.code == _pais;
			});

			if (_isRepeat.length == 0) {
				result = true;
			} else {
				result = false;
			}
			return result;
		},

		// Mostrar las tarjetas para Desktip y Mobile
		generateSelectCreditcard: function (_oData) {
			// Versión Desktop
			for (var i = 0; i < _oData.length; i++) {
				var tmpTarjetas = `<option value="${_oData[i].idCard}">${_oData[i].status}-${_oData[i].cardNumber}</option>`;
				$(tmpTarjetas).appendTo($('#selTarjetas'));
			}
			// Aperturar el Selector
			$('#selTarjetas').niceSelect();

			// Versión Mobile
			for (var j = 0; j < _oData.length; j++) {
				var tmpTarjetasMob = `<div class="inteligo-sidebarbottom-tc-option">
                                <label class="container-radio " for="tc-${_oData[j].idCard}">
                                  <input  type="radio" value="${_oData[j].idCard}" id="tc-${_oData[j].idCard}" name="mob-sel-tarjetas">
                                  <span class="checkmark"></span>
                                  <label for="tc-${_oData[j].idCard}" class="container-radio-text">${_oData[j].status}-${_oData[j].cardNumber}</label>
                                  </label>
                              </div>`;
				$(tmpTarjetasMob).appendTo($('#selTarjetasMob'));
			}
		},
		// Cargar todas las tarjetas
		loadAllCreditCards: function () {
			Global.promiseCreditCard.then(function (_oData) {
				// Aquí ejecutamos las tarjetas de crédito
				var tmpCreditCard = document.getElementById('tmpCreditCard').innerHTML;
				var htmlCreditCard = Mustache.render(tmpCreditCard, _oData);
				$('#mustacheCreditCard').empty().html(htmlCreditCard);

				// Agregamos la cantidad de tarjetas de credito
				$('#mustacheCreditCard').parent().attr('data-total-cards', _oData.datos.cuenta.tarjetas.length);

				// Aquí ejecutamos la INFO general de la tarjeta
				var tmpTarjetaInfo = document.getElementById('tmpTarjetaInfo').innerHTML;
				var htmlTarjetaInfo = Mustache.render(tmpTarjetaInfo, _oData);
				$('#mustacheTarjetaInfo').empty().html(htmlTarjetaInfo);

				// Cargo la data de cada tarjeta: Aquí se ejecuta la 1era tarjeta
				$('#mustacheCreditCard .creditcard-container-card:first-child').trigger('click');
			});
		},
		// Control de la info de cada tarjeta y sus movimientos
		controlDataCards: function (_idCard) {
			// Filtrar objeto según el clic seleccionado y obtener data
			var _oDataCC = Global.oCreditCards.filter(function (oData) {
				return oData.idCard == _idCard;
			});

			// Hacer el pintado de cada nombre de tarjeta
			var tmpNameCC = `<div><i class="icon icon-purchase"></i>
                        ${_oDataCC[0].name} <span style="text-transform: lowercase;">(${_oDataCC[0].status})</span></div>
                       <div>${_oDataCC[0].cardNumber}</div>`;
			$('#infoCreditCard').empty();
			$(tmpNameCC).appendTo($('#infoCreditCard'));

			// LLamamos Servicio a Movimientos de la TC
			Global.promiseTCmovimientos = new Promise(function (resolve, reject) {
				$.ajax({
					url: scopes.urlTCmovimientos, //cambiar aquí
					contentType: 'application/json; charset=utf-8',
					cache: false,
					dataType: 'json',
					type: 'POST',
					async: true,
					crossDomain: true,
					headers: {
						accept: 'application/json',
						'Access-Control-Allow-Origin': '*',
					},
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
					},
				});
			});

			// Ejecutamos funciones al cargar los movimientos
			Global.promiseTCmovimientos.then(function (_oData) {
				// Limpiamos la tabla
				$('#mustacheCreditcardMovimientos').empty();
				var trTable = "<tr> <td class='creditcard-table-td-msj'> <p>Cargando ...</p>  </td> </tr> ";
				$(trTable).appendTo($('#mustacheCreditcardMovimientos'));

				// Acciones temporales por fines del Mock
				var oData = [];
				var time = 0;

				if (_idCard == '00001') {
					oData = _oData.datos.movimientos['00001'];
					time = 500;
				} else if (_idCard == '00002') {
					oData = _oData.datos.movimientos['00002'];
					time = 400;
				} else if (_idCard == '00003') {
					oData = _oData.datos.movimientos['00003'];
					time = 800;
				} else {
					oData = _oData.datos.movimientos['00004'];
					time = 1200;
				}
				// end

				// Este setTimeout es solo para simular la carga de la data
				setTimeout(function () {
					// Controlar cuando haya Data y no tenga data
					if (Object.keys(oData.datos).length > 0) {
						// Aquí ejecutamos el Mustache para tabla
						var tmpCreditcardMovimientos = document.getElementById('tmpCreditcardMovimientos').innerHTML;
						var htmlCreditcardTable = Mustache.render(tmpCreditcardMovimientos, oData);
						$('#mustacheCreditcardMovimientos').empty().html(htmlCreditcardTable);

						// Validamos el botone en movimientos : Mostrar mas | Mostrar menos
						if (
							Global.isWidth < 1250 &&
							$('#mustacheCreditcardMovimientos .creditcard-table-tr').length >= scopes.maxMovs
						) {
							$('#mustacheCreditcardMovimientos .creditcard-table-tr').slice(scopes.maxMovs).hide();
							$('.creditcard-table-tfooter').show();
							$('#btnShowMore').text(scopes.txtShowMovs);
						}
						$('#btnShowMore').show();
					} else {
						$('#mustacheCreditcardMovimientos').empty();
						// Aqui ponemos que no hay data
						trTable = "<tr> <td class='creditcard-table-td-msj'> <p>No se encontraron datos</p>  </td> </tr> ";
						$(trTable).appendTo($('#mustacheCreditcardMovimientos'));

						$('#btnShowMore').hide();
					}
				}, time);
			});
		},
		// Controlar el slider que se ejecutará para las tarjetas
		controlSliderCards: function (event) {
			var _sizeCard = $('#mustacheCreditCard .creditcard-container-card:first-child').width();

			// Controlar tamaño, según cantidad de tarjetas

			// Casuísticas con 4 Tarjetas
			if (Global.oCreditCards.length == 4) {
				if (Global.isWidth > 650 && Global.isWidth <= 1500) {
					if ($(event).index() == 3) {
						methods.positionSliderCard(_sizeCard * 3 + 240);
					} else if ($(event).index() == 2) {
						methods.positionSliderCard(_sizeCard * 2 - 140);
					} else if ($(event).index() == 0 || $(event).index() == 1) {
						methods.positionSliderCard(0);
					}
				} else if (Global.isWidth > 450 && Global.isWidth <= 650) {
					if ($(event).index() == 3) {
						methods.positionSliderCard(_sizeCard * 3 + 200);
					} else if ($(event).index() == 2) {
						methods.positionSliderCard(_sizeCard * 2 - 100);
					} else if ($(event).index() == 1) {
						methods.positionSliderCard(_sizeCard - 100);
					} else {
						methods.positionSliderCard(0);
					}
				} else if (Global.isWidth > 380 && Global.isWidth <= 450) {
					if ($(event).index() == 3) {
						methods.positionSliderCard(_sizeCard * 3 + 200);
					} else if ($(event).index() == 2) {
						methods.positionSliderCard(_sizeCard * 2 - 50);
					} else if ($(event).index() == 1) {
						methods.positionSliderCard(_sizeCard - 50);
					} else {
						methods.positionSliderCard(0);
					}
				} else if (Global.isWidth <= 380) {
					if ($(event).index() == 3) {
						methods.positionSliderCard(_sizeCard * 3 + 200);
					} else if ($(event).index() == 2) {
						methods.positionSliderCard(_sizeCard * 2 - 10);
					} else if ($(event).index() == 1) {
						methods.positionSliderCard(_sizeCard - 10);
					} else {
						methods.positionSliderCard(0);
					}
				}
			}

			// Casuísticas con 3 Tarjetas

			if (Global.oCreditCards.length == 3) {
				if (Global.isWidth > 450 && Global.isWidth <= 800) {
					if ($(event).index() == 2) {
						methods.positionSliderCard(_sizeCard * 2 + 100);
					} else if ($(event).index() == 1) {
						methods.positionSliderCard(_sizeCard - 100);
					} else {
						methods.positionSliderCard(0);
					}
				} else if (Global.isWidth > 380 && Global.isWidth <= 450) {
					if ($(event).index() == 2) {
						methods.positionSliderCard(_sizeCard * 2 + 100);
					} else if ($(event).index() == 1) {
						console.log('see');
						methods.positionSliderCard(_sizeCard - 50);
					} else {
						methods.positionSliderCard(0);
					}
				} else if (Global.isWidth <= 380) {
					if ($(event).index() == 2) {
						methods.positionSliderCard(_sizeCard * 2 + 100);
					} else if ($(event).index() == 1) {
						methods.positionSliderCard(_sizeCard - 10);
					} else {
						methods.positionSliderCard(0);
					}
				}
			}

			if (Global.oCreditCards.length == 2 && Global.isWidth < 800) {
				if ($(event).index() == 1) {
					methods.positionSliderCard(_sizeCard * 2 + 100);
				} else {
					methods.positionSliderCard(0);
				}
			}
		},
		positionSliderCard: function (_leftCard) {
			setTimeout(function () {
				document.getElementById('mustacheCreditCard').scroll({
					left: _leftCard,
					top: 0,
					behavior: 'smooth',
				});
			}, 120);
		},
	};

	var initialize = function () {
		subsEvents();
		services.loadPromises();
	};

	return {
		init: initialize,
	};
})();

window.addEventListener(
	'load',
	function () {
		creditcard.init();
	},
	false,
);
