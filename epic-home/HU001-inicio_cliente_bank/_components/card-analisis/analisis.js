var card_analisis = (function () {
  var scopes = {
    // urlAnalisis    : "https://demo6027168.mockable.io/analisis",
    urlAnalisis    : "_components/card-analisis/mocks/analisis.json",
    urlSabermas    : "_components/card-analisis/mocks/sabermas.json"
  }

  var services = {
    loadPromises : function() {
      // Cargar todo los Análisis
      Global.promiseAnalisis = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlAnalisis,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });


      // Cargar todo los Saber más
      Global.promiseSaberMas = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlSabermas,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });
    }
  }

  var subsEvents = function(){  
    // Evento para probar el loading-skeleton 
    $('#selAnalysisPortfolio').on("change", events.testSkeleton)
    // Abrir modal de "Saber más"
    $('#modalPatrimonio').on("click", function(){
      var id = $(this).attr("data-modal")
      Global.modalOpen("#"+id)
    })
    // Abri modal de contactar
    $('#btnOpenContactar').on("click", function(){
      Global.modalOpen("#modal-inteligo-contact")
    })
  };
  
  var events = {
    // Simulación al dar clic para ver la interacción del Skeleton
    testSkeleton: function () {
      setTimeout(function () {
        Global.showSkeleton("#cardAnalisis");
        setTimeout(function () {
          Global.hideSkeleton("#cardAnalisis")
        }, 1200)
      }, 200)      
    },
    // Crear personalizar dentro del footer en mobile
    detectMobile: function () {
      if ( Global.isMobile() ) {      
        if ( $(window).width() < 650 ) {
          // $('#blockValorPortafolio').detach().insertAfter('#blockPortafolio');
        }

      }
    },
   
    
  };
  var methods = {
    loadSelects : function() {
      var $selAnalysisPortfolio = $('#selAnalysisPortfolio');
      $selAnalysisPortfolio.niceSelect();

      // Despues del elegir opción, se carga el gráfico
      methods.loadTable()
      methods.loadGraphics()      
    },
    loadTable: function() {
      Global.promiseAnalisis.then(function(_oData){

        // Agregar clases de estados para entorno mobile
        for(var i=0 ; i < _oData.datos.analisis.length ; i++){
          
          if(_oData.datos.analisis[i]["status"] == true){
            _oData.datos.analisis[i]["statusClass"] = "analisis-mstatus-success"
          }else{
            _oData.datos.analisis[i]["statusClass"] = "analisis-mstatus-error"
          }
        }

        // Aquí ejecutamos el Mustache para renderizar la tabla
        var template1 = document.getElementById('tmpAnalisisTable').innerHTML;
        var rendered1 = Mustache.render(template1, _oData );
        $('#mustacheAnalisisTable').empty().html(rendered1);

        // Aquí ejecutamos el Mustache para renderizar la legenda
        var template2 = document.getElementById('tmpAnalisisLegends').innerHTML;
        var rendered2 = Mustache.render(template2, _oData );
        $('#mustacheAnalisisLegends').empty().html(rendered2);

        // Aquí ejecutamos el Mustache para renderizar el gráfico
        // var template3 = document.getElementById('tmpAnalisisGraphic').innerHTML;
        // var rendered3 = Mustache.render(template3, _oData );
        // $('#mustacheAnalisisGraphic').empty().html(rendered3);

        // Aquí ejecutamos el Mustache para renderizar la Table Mobile
        var template4 = document.getElementById('tmpAnalisisTableMobile').innerHTML;
        var rendered4 = Mustache.render(template4, _oData );
        $('#mustacheAnalisisTableMobile').empty().html(rendered4);

      })
    },
    loadGraphics: function () {
      Global.AnalisisPortOptions = {
        "title": {
            "text": ""
        },
        "subtitle": {
            "text": ""
        },
        "exporting": {
            "enabled": false
        },
        "chart": {
            "height": 150,
            "width": 150,
            "type": "pie",
            "polar": false,
            "options3d": {
                "fitToPlot": true
            }
        },
        "plotOptions": {
            "pie": {
                "allowPointSelect": true,
                "cursor": true,
                size: '100%',
                "showInLegend": false,
                "innerSize": "70%",
                "dataLabels": {
                    "enabled": false
                },
                "tooltip": {
                    "animation": true
                },
                "states": {
                    "select": {
                        "enabled": true,
                        "halo": {
                            "size": 0
                        },
                        "marker": {
                            "states": {
                                "hover": {
                                    "enabled": true
                                }
                            }
                        },
                        "brightness": 0
                    },
                    "hover": {
                        "halo": {
                            "size": 4,
                            "opacity": 0.6,
                            "duration": 1000
                        },
                        "brightness": 0,
                        "enabled": true
                    },
                    // "inactive": {
                    //     opacity: 1
                    // },
                },
                "getExtremesFromAll": false,
                "ignoreHiddenPoint": true,
                "slicedOffset": 10
            }
        },
        "series": [{
            data : [],
            "name": "Column 2",
            "turboThreshold": 0,
            "wrap": true,
            "visible": true,
            "yOffset": -20,
            "slicedOffset": 0,
            "softThreshold": true
        }],
        "yAxis": [{
            "title": {
                "text": ""
            }
        }],
        "legend": {
            "enabled": false
        },
        "xAxis": [{}],
        "tooltip": {
            "enabled": true,
            "shared": false,
            "pointFormat": "",
            "backgroundColor": "#2765F5",
            "borderColor": "#2765F5",
            "borderRadius": 2,
            "headerFormat": "<span style=\"font-size: 12px; color: #ffffff; text-align: center;\">{point.key}</span>",
            "animation": true,
            "cursor": 'pointer'
        },
        "pane": {
            "background": []
        },
        "responsive": {
            "rules": []
        },
        "credits": {
            "enabled": false,
            "text": "",
            "href": ""
        },
        colors: ["#80CBC4", "#00C853", "#01579B", "#FFAB00"]
      };

      // Gráfico 1
      Global.AnalisisPortActual = Highcharts.chart('card3Graph', Global.AnalisisPortOptions);
      Global.AnalisisPortActual.series[0].update({
        data: [ ["Cash y equivalentes",25],["Renta fija",46],["Renta variable",10],["Inv. alternativas",19]]//agregar más valores al gráfico aquí
      })

      // Gráfico 2
      Global.AnalisisPortRecomend = Highcharts.chart('card3Graph1', Global.AnalisisPortOptions);
      Global.AnalisisPortRecomend.series[0].update({
        data: [ ["Cash y equivalentes",10],["Renta fija",35],["Renta variable",35],["Inv. alternativas",20]]
      })

      // Gráfico 3 ( Saber más )
      Global.promiseSaberMas.then(function(_oData){
        // Crear modal de SAber más: id template,  datos de servicio y  config de plugin
        Global.modalInitialize( 4,  _oData.datos.sabermas, Global.AnalisisPortOptions );

        // Agregar el ID al boton que llamara la modal
        var idButton = "modal-template4-" +_oData.datos.sabermas.id;
        $('#modalPatrimonio').attr("data-modal", idButton )
      })

    }
  }

  var initialize = function () {
    subsEvents();
    services.loadPromises()
    events.detectMobile()
    methods.loadSelects()

    


  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    card_analisis.init();
  },
  false
);