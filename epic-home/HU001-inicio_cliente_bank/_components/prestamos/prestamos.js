/*
 * Préstamos
 * @author Mfyance
 */

var prestamos = (function () {
  var scopes = {
    urlModalPrestamos   : "_components/prestamos/mocks/modal.json",
    urlPrestamos        : "_components/prestamos/mocks/prestamos.json",
    urlPrestamoDetalle  : "_components/prestamos/mocks/prestamo-detalle.json",
    urlCronograma       : "_components/prestamos/mocks/cronograma.json"
  }

  var services = {
    loadPromises : function() {

      // Llamamos Servicio de datos de la Modal
      Global.promiseModalPrestamo = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlModalPrestamos,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });
      
      // Generar Modal de Aviso Viaje
      Global.promiseModalPrestamo.then(function(_oData){
        // oExtra es una función que se ejcutará posterior a la creación de la Modal
        var oExtra = function() {

          // Configuracion de los switch de la modal
          $('.inteligo-config-switch').on("change", function(){
            // Deshabilitar el boton principal
            $(this).closest(".inteligo-modal-main").find(".button-primary").removeAttr("disabled")

            if($(this).closest(".inteligo-modal-block").find(".modal-input-config").length > 0 ){              
              if($(this).is(":checked")){
                $(this).closest(".inteligo-modal-block").find(".modal-input-config").removeAttr("disabled")
              }else{
                $(this).closest(".inteligo-modal-block").find(".modal-input-config").attr("disabled", true)
                $(this).closest(".inteligo-modal-block").find(".modal-input-config").val("1")
              }
            }
          })

          // Pintar correo enmascarado
          var _email = Global.maskMail($('#inpEmailConfig').val())
          printEmail(_email)
          
          // Función para pintar el Email enmascarado
          function printEmail(_email){
            $('[data-email="emailMask"]').text(_email)
          }

          // Cuando doy clic en guardar nueva contraseña
          $('#inteligoContentModals').on("click", "#btnUpdateMail", function(){
            
            // Pintar el nuevo correo
            var _newEmail = $('#inpEmailConfig').val()
            printEmail(_newEmail)

            // Eventos para ocultar los bloques dentro de la Modal
            $(this).closest(".inteligo-modal").find('[data-modal="step-1"]').show()
            $(this).closest(".inteligo-modal").find('[data-modal="step-2"]').hide()

            // Limpiar campos form
            setTimeout(function(){
              $('#newEmail').val("")
              $('#newEmailRepeat').val("")
              $('#btnUpdateMail').attr("disabled", true)
            }, 400)
          })

          // Validar que las 2 email sean iguales
          $('#inteligoContentModals').on("keypress keyup", "#newEmail, #newEmailRepeat", function(){
            var _email1 = $('#newEmail').val() 
            var _email2 = $('#newEmailRepeat').val() 

            if(Global.isMail(_email1) == true && Global.isMail(_email2) == true){
               if (_email1 != _email2 || _email1 == "" || _email2 == "") {
                $('#btnUpdateMail').attr("disabled", true)

                 $('#newEmail').closest(".inteligo-input-container").addClass("error")
                 $('#newEmailRepeat').closest(".inteligo-input-container").addClass("error")
                 if($('#newEmailRepeat').closest(".inteligo-input-container").find("label").length <= 0) {
                  $('#newEmailRepeat').closest(".inteligo-input-container").append("<label>Los correos no coinciden entre sí. Por favor, verificarlos.<label>")
                 }

               }else{
                $('#newEmail').closest(".inteligo-input-container").removeClass("error")
                $('#newEmailRepeat').closest(".inteligo-input-container").removeClass("error")
                $('#newEmailRepeat').closest(".inteligo-input-container").find("label").remove()
                $('#btnUpdateMail').removeAttr("disabled")
               }
            }else{
              $('#btnUpdateMail').attr("disabled", true)
            }
          })

          // Cuando guardo las opciones
          $('#inteligoContentModals').on("click", "[data-modal='save']", function(){
            var _id = $(this).closest(".inteligo-modal").attr("id")
            // Ejecutar alguna función de guardado
            Global.oDataModal = []

            $('#'+_id +' *').filter('input[type="checkbox"]').each(function (i,e) {    
              if($(this).attr("name") != undefined ){
                Global.oDataModal.push({
                  value : $(this).is(":checked"),
                  name :$(this).attr("name")
                })
              }
            })
            $('#'+_id +' *').filter('input[type="text"]').each(function (i,e) {    
              if($(this).attr("name") != undefined ){
                Global.oDataModal.push({
                  value : $(this).val(),
                  name :$(this).attr("name")
                })
              }
            })

            // Agregar el dato del correo
            Global.oDataModal.push({
              value: $('#inpEmailConfig').val(),
              name : $('#inpEmailConfig').attr("name")
            })
            console.log('----Data de: "'+_id+'"--------')
            console.log(Global.oDataModal)

            // Cerrar la modal luego de enviar data
            Global.modalClose("#"+_id)
          })
        }

        Global.modalInitialize(9,_oData, oExtra)
      })

      // Cargar data de Prestamos Principal
      Global.promiseAllPrestamos = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlPrestamos,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });

      // Ejecutar tabla de Prestamos Principal
      Global.promiseAllPrestamos.then(function(_oData){
        methods.printAllPrestamos(_oData)
      });

      // Cargar data de Prestamo Detalle
      Global.promisePrestamo = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlPrestamoDetalle,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });


      // Ejecutar tabla de Prestamo Detalle
      Global.promisePrestamo.then(function(_oData){

        // Pintar Cabecera Préstamos
        methods.printHeadPrestamo(_oData)

        // Llamar el cronograma
        $.ajax({
          url: scopes.urlCronograma,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (_oData) {
            Global.oDataCronograma = _oData.datos;
            // Activar en la primera carga la "Lista de pendientes"
            // Al agregar el 0,  le digo q sera la primera carga
            events.tableCronograma("pendiente", 0)
          }
        })

        
      });
    }
  }

  var subsEvents = function(){
    $('#mustachePrestamo').on("click", '#btnOpenConfig', function(){
      // Abrir modal de Config
      Global.modalOpen('#modal-prestamos')
    })

    // Event: Filtro de cronogramas
    $('#mustachePrestamo').on("change", '[name="inpCuotas"]', function(){
      var _cuotas = $(this).val()
      events.tableCronograma(_cuotas)
    })

    // Event:: Formatear texto
    $('#modal-inteligo-email .inteligo-modal-close').on("click", function(){
      $('#inpSendmailAsunto').val("")
    })

    // Event: Ver detalle de cada Préstamo
    $('#mustacheAllPrestamos').on("click", '[data-prestamos]', events.showDetailPrestamos)
   
    // Event: Enviar Email con Asunto
    $('#mustachePrestamo').on("click", '[data-modal="email"]', events.sendEmailPrestamos)

    // Event: Colapsar tabla en version responsive
    $("#mustachePrestamo").on("click", '.mob-portafolio-arrow' , events.slideTable);
  };
  
  var events = {
    // Colapsar tabla responsive
    slideTable: function () {
      $(this).closest("table").toggleClass("disabled")
    },
    // Obtener el ID del préstamos donde hice clic
    showDetailPrestamos: function(){
      Global.idPrestamo = $(this).attr("data-prestamos")
      console.log("Préstamo: "+Global.idPrestamo)

      // Aqui se debe ejcutar servicios para traer data por ID Préstamos
    },
    // Evento para enviar Email con Asunto
    sendEmailPrestamos: function(){      
      var _id = Global.idPrestamo;
      if(_id != undefined){
        var _text = "Información sobre Préstamo"
        var _date = moment().format('DD MMM YYYY');
        Global.modalAsuntoShow(_text+" "+ _id + " al "+_date)
      }      
    },
    tableCronograma: function(_cuotas, _first){
      switch(_cuotas) {
        case "pendiente":          
          // Pintar tabla de cronograma "PENDIENTE"
          var _oDataCronograma = Global.oDataCronograma.cronograma.filter(function(oData) {
            return oData.status == "pendiente";
          })
          var _tableCronograma = { "cronograma": _oDataCronograma }
          methods.printTableCronograma(_tableCronograma)
          setTimeout(function(){

            // En caso de la primera carga para PENDIENTE
            if(_first == 0){
              $('#pendiente').attr("checked", true)
              $('#pendiente').prop("checked", true)
              $('#pendiente').trigger("change")
            }
            

            // Agregar estilos para la tabla
            $('#partialCronograma').find("#prestamosTableDesktop table").attr("data-prestamos", "pendiente")
            // Contador de cantidad de opciones
            if(_oDataCronograma.length <= 1){
              $('#partialCronograma').find("#prestamosTableMobile .mob-portafolio-table").removeClass("disabled")
            }
          }, 50)
          break;
        case "pagada":          
          // Pintar tabla de cronograma "PAGADA"
          var _oDataCronograma = Global.oDataCronograma.cronograma.filter(function(oData) {
            return oData.status == "pagada";
          })
          var _tableCronograma = {   "cronograma": _oDataCronograma } 
          methods.printTableCronograma(_tableCronograma)
          setTimeout(function(){
            // Agregar estilos para la tabla
            $('#partialCronograma').find("#prestamosTableDesktop table").attr("data-prestamos", "pagada")
            // Contador de cantidad de opciones
            if(_oDataCronograma.length <= 1){
              $('#partialCronograma').find("#prestamosTableMobile .mob-portafolio-table").removeClass("disabled")
            }
          }, 50)    
          break;
        default:
          methods.printTableCronograma(Global.oDataCronograma)       
          setTimeout(function(){
            // Agregar estilos para la tabla
            $('#partialCronograma').find("#prestamosTableDesktop table").attr("data-prestamos", "todos")
          }, 50)
      }        
    }
  };

  var methods = {
    // Pintar todos los prestamos
    printAllPrestamos : function(_data){
      var tmpAllPrestamos = document.getElementById('tmpAllPrestamos').innerHTML;
      var htmlPrestamos = Mustache.render(tmpAllPrestamos, _data );
      $('#mustacheAllPrestamos').empty().html(htmlPrestamos);
    },
    // Pintar la data de los cronogramas segun su filtro
    printTableCronograma : function(_data){
      var tmpCronograma = document.getElementById('tmpPrestamoCronograma').innerHTML;
      var htmlCronograma = Mustache.render(tmpCronograma, _data );
      $('#partialCronograma').empty().html(htmlCronograma);
    },    
    // Pintar el detalle de un préstamo Head 
    printHeadPrestamo: function(_data){
      var tmpPrestamo = document.getElementById('tmpPrestamo').innerHTML;
      var tmpPartialCronograma = document.getElementById('tmpPrestamoCronograma').innerHTML;
      var htmlPrestamo = Mustache.render(tmpPrestamo, _data, {partialCronograma: tmpPartialCronograma} );
      $('#mustachePrestamo').empty().html(htmlPrestamo);
    }

  }

  var initialize = function () {
    subsEvents();
    services.loadPromises();
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    prestamos.init();
  },
  false
);
