
/*
 * Trading
 * @author José, Diego, Mfyance
 */

var cta_trading = (function () {
  var scopes = {
    urlTradingDetalle   : "_components/cta-trading/mocks/trading-detalle.json",
    urlTrading          : "_components/cta-trading/mocks/trading.json",
    urlMovimientos      : "_components/cta-trading/mocks/movimientos.json"
  }

  var services = {
    loadPromises : function() {
      // Llamamos Servicio Detalle Trading
      Global.promiseTrading = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlTradingDetalle,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });

      Global.promiseTrading.then(function(_oData){
        // Pintar Cabecera Trading
        methods.printTrading(_oData);

        // Refrescar tabla con movimientos segun Filtro
        var _filter = $('#filterTrading').val()
        var firstMovement = services.loadMovements(_filter)

        // Callback al tener data para la tabla
        firstMovement.then(function(_oData){        
          methods.printTableMovimientos(_oData.datos)
        });        
      
      })

      // Cargar data de Trading Principal
      Global.promiseAllTrading = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlTrading,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });

      // Ejecutar tabla de Trading Principal
      Global.promiseAllTrading.then(function(_oData){
        methods.printAllTrading(_oData)       
      });     
    },
    loadMovements: function(_filter){
      var oMovements = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlMovimientos,
          contentType: 'application/json; charset=utf-8',
          cache: false,
          dataType: "json",
          type: 'POST',
          async: true,
          crossDomain: true,
          headers: {
            "accept": "application/json",
            "Access-Control-Allow-Origin":"*"
          },
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });
      return oMovements
    }
  }

  var subsEvents = function(){
    // Event: Clic en el botón Buscar
    $('#mustacheTrading').on("click", '#btnSearchMovements', events.refreshMovements)

    // Event: Mostrar Calendario personalizado
    $('#mustacheTrading').on("change", '#filterTrading', events.openCustomizeCalendar)
    $('#mustacheTrading').on("change", '#filterTrading', events.refreshMovements)
    
    // Event: Calcular Saldo
    $('#mustacheAllTrading').on("change", '[name="trading-saldo"]',  events.calculateSaldo);

    // Event: Switch de Monedas
    $(".left-button").on("click", function() {
      $(".left-button").addClass("active");
      $('.trading-cards-title').css('color','#233E99');
      $('.trading-cards-subtitle').text(function(_,txt) {
        return txt.replace('EUR','USD')
      });
      $(".right-button").removeClass("active");
    });

    // Event: Cambio de monedas
    $(".right-button").on("click", function() {
      $(".right-button").addClass("active");
      $('.trading-cards-title').css('color','#126B62');
      $('.trading-cards-subtitle').text(function(_,txt) {
        return txt.replace('USD','EUR')
      });
      $(".left-button").removeClass("active");
    });

    // Event: Ver detalle de cada Trading
    $('#secAllTradings').on("click", '[data-trading]', events.showDetailTrading)
    $('#menuPatrimonioTotal').on("click", '[data-menu-trading]', events.showDetailTrading)

    // Event: Enviar Email con Asunto
    $('#secCtaTrading').on("click", '[data-modal="email"]', events.sendEmailTrading)

    // Event: Resetear el calendario al valor por defecto
    $('body').on("click", '[data-section="secCtaTrading"]', function(){
      Global.reloadSelect('#filterTrading', 1)
    });
  };
  
  var events = {
    // Recargar la tabla de Movimientos
    refreshMovements: function(event){

      // Si hace el filtro desde el Selector y omitiendo el Personalizado
      if($(event.target).is("#filterTrading") && $(event.target).val() != 4){
        var _filter = $(this).val()
        // console.log(_filter + " <- valor filtro") 

        // Resetear valores de Calendarios
        methods.resetFilterCalendar()
      }
      // Si hace el filtro desde el botón
      else if ($(event.target).is("#btnSearchMovements")){
        var _filter = {
          "fecha-ida": ($('#monthpicker3').val() != "") ? $('#monthpicker3').val() : 0,
          "fecha-vuelta":  ($('#monthpicker4').val() != "") ? $('#monthpicker4').val() : 0
        }
        // console.log(JSON.stringify(_filter) + " <- valor filtro")
      }

      // Refrescar tabla con movimientos segun Filtro
      var newMovements = services.loadMovements(_filter)

      // Callback al tener data para la tabla
      newMovements.then(function(_oData){        
        methods.printTableMovimientos(_oData.datos)
      });
    },   
    // Obtener el ID del Trading donde hice clic
    showDetailTrading: function(){
      if($(this).attr("data-menu-trading")){
        Global.idTrading = $(this).attr("data-menu-trading")
      }else{
        Global.idTrading = $(this).attr("data-trading")
      }
      console.log("Trading: "+Global.idTrading)

      // Aqui se debe ejcutar servicios para traer data por ID Préstamos
    },
    // Evento para enviar Email con Asunto
    sendEmailTrading: function(){
      var _id = Global.idTrading;
      if(_id != undefined){
        var _text = "Información sobre Cuenta trading"
        var _date = moment().format('DD MMM YYYY');
        Global.modalAsuntoShow(_text+" "+ _id + " al "+_date)
      }      
    },
    // Calcular el Saldo según los Checks que correspondan
    calculateSaldo: function(){
      var sum = 0;
      $('[name="trading-saldo"]:checked').each(function() {
        sum += parseFloat($(this).closest('.table-resumen-body-col').find('.table-resumen-body-col-subtitle').text().replace(/[^\d.-]/g, ''));
      });
      if (sum > 0) {
        // Agregar separador de miles y millones
        sum = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        // Redondear y mostrar 2 decimales o mostrar 00 precedido de un punto
        if (sum.indexOf('.') !== -1) {
          sum = parseFloat(sum).toFixed(2);
        } else {
          sum = sum + '.00';
        }
        // Mostrar la suma en el elemento con clase "table-resumen-amount-money"
        $('.table-resumen-amount-money').text('USD $' + sum);
      } else {
        // Mostrar 0.00 en el elemento con clase "table-resumen-amount-money"
        $('.table-resumen-amount-money').text('USD 0.00');
      }
    },
    // Mostrar Calendario
    openCustomizeCalendar: function(){
      if($(this).val() == 4){
        $('#tradingCalendar').show()
      }else{
        $('#tradingCalendar').hide()
      }
		}
  };

  var methods = {
    // Pintar la data de los Movimientos segun su filtro
    printTableMovimientos : function(_data){
      var tmpMovimientos = document.getElementById('tmpTradingMovimientos').innerHTML;
      var htmlMovimientos = Mustache.render(tmpMovimientos, _data );
      $('#partialMovimientos').empty().html(htmlMovimientos);
    },
    // Pintar data de Movientos
    printTrading: function(_data){
      var tmpTrading = document.getElementById('tmpTrading').innerHTML;
      var tmpPartialMovimientos = document.getElementById('tmpTradingMovimientos').innerHTML;
      var htmlTrading = Mustache.render(tmpTrading, _data, {partialMovimientos: tmpPartialMovimientos} );
      $('#mustacheTrading').empty().html(htmlTrading);

      // Ejecutar calendario
      Global.runSelect("#filterTrading")

      // Ejecutar eventos del Calendario
      Global.loadJS("../../static/libs/datepicker/custom.js")
    },
    printTableTrading: function(){
      var tmpCronograma = document.getElementById('tmpPartialMovimientosTrading').innerHTML;
      var htmlCronograma = Mustache.render(tmpCronograma, _data );
      $('#mustacheTradingTable').empty().html(htmlCronograma);
    },   
    // Pintar todos los Tradings
    printAllTrading: function(_data){
      var tmpAllTrading = document.getElementById('tmpAllTrading').innerHTML;
      var htmlAllTrading = Mustache.render(tmpAllTrading, _data );
      $('#mustacheAllTrading').empty().html(htmlAllTrading);
    },
    // Poner el calendario en el valor por defecto
    resetFilterCalendar: function(){
      $('#monthpicker3').val("")
      $('#monthpicker4').val("")
    }
  }

  var initialize = function () {
    subsEvents();
    services.loadPromises()
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    cta_trading.init();
  },
  false
);