/*
 * Portafolios
 * @author mfyance
 */




var card_portafolios = (function () {
  var scopes = {
    urlPortfolios       : "_components/card-portafolio/mocks/lista-portfolios.json",
    urlDetail           : "_components/card-portafolio/mocks/detalle-deposición.json",
    urlRentability      : "_components/card-portafolio/mocks/rentabilidad-barra.json",
    urlEvolution        : "_components/card-portafolio/mocks/evolucion-historica.json",
    // urlPortfolios    : "https://demo6027168.mockable.io/lista-portafolios",
    // urlDetail        : "https://demo6027168.mockable.io/detalle-deposicion",
    // urlRentability   : "https://demo6027168.mockable.io/rentabilidad-barra",
    // urlEvolution     : "https://demo6027168.mockable.io/evolucion-historica",
    // urlPersonalize   : "https://demo6027168.mockable.io/lista-personalizar",
  };
  var subsEvents = function(){  
    $('#selPortfolios').on("change", events.loadDetectPortfolios );
    $('#selPortfolios').on("change", events.loadDetectPortfolios );
    $('#loadContentTabs [data-tab]').on("click", function(){
      // Refrescar contenedor de rentabilidad
      events.loadDetectPortfolios();
    })
    $("#mustachePersonalizar ").on("click", "input"  ,events.controlPersonalize );
    $('#btnPersonalizar').on("click", events.openPersonalize );
    $('.mob-portafolio-arrow').on("click", events.slideTable);
    $('#btnPersoDeseleccionar').on("click", events.deselecPersonalizar);
    $('#btnPersoAplicar').on("click", events.reordenPersonalizar);
    $('#btnClosePersonalize').on("click", events.closePersonalize);
    $('#layoutBackdrop').on("click", events.hideBackdrop);
    $(document).on("click", events.controlClicDocument);

    // Evento para probar el loading-skeleton 
    $('[data-tab-position="tabTipoProducto"]').on("click", events.testSkeleton)
    
    // Cambio en los radio buttons
    $("[name=resumen-filtro]").on("change", events.controlResumen)
  };

  var events = {
    // Simulación de las tablas
    controlResumen: function () {
      var valueRadio = $(this).val();
      if(valueRadio == "resumen-clase"){
        $('#tblDetPosicionActivoDesktop .inteligo-table-thead-th:nth-child(4)').hide()
        $('#tblDetPosicionActivoDesktop .inteligo-table-tbody-td:nth-child(4)').hide()
        $('#tblDetPosicionActivoDesktop .inteligo-table-tfoot-td:nth-child(4)').hide()
      }else{
        $('#tblDetPosicionActivoDesktop .inteligo-table-thead-th:nth-child(4)').removeAttr("style")
        $('#tblDetPosicionActivoDesktop .inteligo-table-tbody-td:nth-child(4)').removeAttr("style")
        $('#tblDetPosicionActivoDesktop .inteligo-table-tfoot-td:nth-child(4)').removeAttr("style")
      }      
    },
    // Simulación al dar clic para ver la interacción del Skeleton
    testSkeleton: function () {
      setTimeout(function () {
        Global.showSkeleton("#cardPortafolios");
        setTimeout(function () {
          Global.hideSkeleton("#cardPortafolios")
        }, 1200)
      }, 200)      
    },
    // Crear personalizar dentro del footer en mobile
    detectMobile: function () {
      if ( Global.isMobile() ) {
        $(".personalizar-main").detach().appendTo('footer');
        $("html").addClass("inteligo-personalizar")
      }
    },
    // Controlar clic en documento y ocultar elementos
    controlClicDocument: function (event) {

      // Calcular barras del card2     
      Global.calculateBars()      

      // Ocultar Pesonalizar
      if($(event.target).closest(".personalizar").length === 0) {
        $(".personalizar").removeClass("active")
      }
    },
    // Esconder layout oscuro
    hideBackdrop : function () {
      $('.inteligo-personalizar').removeClass("open")
    },
    // Volver a cerrar el toolbar de Personalizar
    closePersonalize: function(){     
      if($('body').hasClass('inteligo-mobile') == true){
         $('html').removeClass("open");
       }else{
        $('.personalizar').removeClass("active")
       }
    },
    // Reordenar las opciones de Personalizar
    reordenPersonalizar: function () {
      // Aqui debería ir la función para recepcionar valores 
      var checkeds = []
      $('[name=personalizar]:checked').each(function (i) { 
        var status = (this.checked ? $(this).val() : ""); 
        var id = $(this).attr("id"); 
        checkeds[i] = id
      });

      // Enviar los elementos checkeados a la función para actualizar las tablas
      methods.setDataPosition(checkeds)


      // Eventos para ocultar la lista de checkbox
      $('.personalizar').removeClass("active");
      if($("body").hasClass("inteligo-mobile")){
        $('html').removeClass("inteligo-personalizar")
      }else{
        $('html').removeClass("open")
      }


    },
    // Deseleccionar todas las opciones de Personalizar
    deselecPersonalizar: function () {
      $(this).attr("disabled", true)
      $('#counterPersonalizar').text("0")
      var totalBoxes = $('#mustachePersonalizar input');
      $('#btnPersoAplicar').attr("disabled", true)  
      for (var e = 0; e < totalBoxes.length; e++){
        totalBoxes[e].disabled = false;
        totalBoxes[e].checked = false;
      }
    },
  
    // Colapsar tabla responsive
    slideTable: function () {
      $(this).closest("table").toggleClass("disabled")
    },
    // Renderizar data de los gráficos dinámicos según el Selector
    loadDetectPortfolios : function(){
      // Obtener Num de portafolio
      var numPortfolio = $("#selPortfolios").val();
      
      // Setear grafico de Rentabilidad según el ID del portafolio
      methods.setDataRentability(numPortfolio);

      // Setear grafico de Evolución Histórica según el ID del portafolio
      methods.setDataEvolutionHistoric(numPortfolio);
    },
    // Abrir el Personalizar
    openPersonalize: function () {
      if($("html").hasClass("inteligo-personalizar") == true){
        $(".inteligo-personalizar").addClass("open")
      }else{
        $(this).parent().toggleClass("active")
      }

      
    },
    // Controlar cantidad de checks a lo de Personalizar
    controlPersonalize : function(event){

      var totalBoxes = $('#mustachePersonalizar input');
      var totalCheck = $('#mustachePersonalizar input').filter(':checked').length;

      // Cantidad de checkeados
      $('#counterPersonalizar').text(totalCheck)

      if ( totalCheck >= 3){
        $('#btnPersoAplicar').removeAttr("disabled")  
        for (var e = 0; e < totalBoxes.length; e++){
          if (totalBoxes[e].checked === false) {
            totalBoxes[e].disabled = true;
          }
        }       
      }

      else{
        $('#btnPersoAplicar').attr("disabled", true)  
        for (var e = 0; e < totalBoxes.length; e++){
          totalBoxes[e].disabled = false;
        }

        if( totalCheck == 0 ) {
          $('#btnPersoDeseleccionar').attr("disabled", true)
        }else{
          $('#btnPersoDeseleccionar').removeAttr("disabled")
        }
        
      }
    }
  };
  var services = {
    
    // Obtener datos para el Selector de Portafolios
    getSelectPortfolios : function (_oData) {
      // Guardar en un promesa los valores del servicio de Portafolios
      Global.promisePortfolio = new Promise(function(resolve, reject) {
        $.ajax({
          url: scopes.urlPortfolios,
          type: 'POST',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          crossDomain: true,
          headers: { 'Content-Type': 'application/json'},
          success: function (data) {
            resolve(data)
          },
          error: function (error) {
            reject(error)
          }
        })
      });

      
      Global.promisePortfolio.then(function(){
        // CARD1 : Llenar datos a traves de Servicios
        methods.loadComponentsCardOne()
      })
      
    },
    // Obtener datos para Detalle de Posicion
    getDataPosition : function (_oData) {


    },
    // Obtener datos para gráfico de Rentabilidad del Portafolio
    getDataRentability : function (_oData) {
      var ajaxRentability = $.ajax({
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        crossDomain: true,
        headers: { 'Content-Type': 'application/json'},
        url: scopes.urlRentability,
        timeout: 30000,
        beforeSend: function(xhr) {            
        }
      }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Solicitud fallida: " + textStatus);
      });
      _oData.call(this, ajaxRentability);
    },

    // Obtener datos para gráfico de Evolución Histórica de Portafolio
    getDataEvolutionHistoric : function (_oData) {
      var ajaxEvolutionHistoric = $.ajax({
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        crossDomain: true,
        headers: { 'Content-Type': 'application/json'},
        url: scopes.urlEvolution,
        beforeSend: function(xhr) {            
        }
      }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log("Solicitud fallida: " + textStatus);
      });
      _oData.call(this, ajaxEvolutionHistoric);
    }
  }

  var methods = {
    // CARD1: Cargar componentes que vienen de un Servicio
    loadComponentsCardOne : function () {
     
      // Cargar lista de portafolios
      Global.promisePortfolio.then(function(_oData) {
        var $selPortfolios = $('#selPortfolios')
        $.each(_oData.datos.cuenta, function(_oKey, _oName) {
          $selPortfolios.append('<option value=' + _oName.nomcta + '>Portafolio ' + _oName.nomcta + '</option>');
        });

        // Ejecutar plugin para el select
        $selPortfolios.niceSelect();
      });

      // Cargar tabla de Detalle de Posición
      Global.promisePortfolio.then(function(_oData) {
      });

      // Cargar Gráfico de Rentabilidad
      Global.promisePortfolio.then(function(_oData) {
        // Ejecutar plugin de ApexCharts
        var oConfigApexChart = {
          
          series: [],
          grid: {
             show: false
          },
          chart: {            
            height: 150,
            redrawOnParentResize: true,
            redrawOnWindowResize: true,
            type: 'bar',
            toolbar: { 
              show: false 
            },
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                  enabled: true,
                  delay: 150
              },
              dynamicAnimation: {
                  enabled: true,
                  speed: 350
              }
            }
          },
          dataLabels: {
            enabled: false,
          },
          noData: {
            text: 'Cargando...'
          },
          plotOptions: {
            bar: {
              colors: {
                ranges: [{
                  from: 0,
                  to: 90,
                  color: '#80AAF8'
                }, {
                  from: -45,
                  to: 0,
                  color: '#ACC8FA'
                }]
              },
              columnWidth: '53%',
            }
          },
          tooltip: {      
            enabled: true,
            enabledOnSeries: undefined,
            shared: true,
            followCursor: false,
            intersect: false,
            inverseOrder: false,
            custom: undefined,
            fillSeriesColor: false,
            theme: false,   
            custom: function( _oData ) {
              var seriesIndex = _oData.seriesIndex;
              var dataPointIndex = _oData.dataPointIndex;
              return (
                '<div class="chart-tooltip">' +
                '<div class="chart-tooltip-top">' +
                _oData.w.globals.categoryLabels[dataPointIndex] +
                '</div><div class="chart-tooltip-bottom">' +
                _oData.series[seriesIndex][dataPointIndex] +
                ' %</div>' +
                '</div>'
              );
            },
            fixed: {
              enabled: false 
            }       
          },
          xaxis: {
            type: "",
            axisTicks: {
              show: false
            },
            tickPlacement: 'on',
            labels: {
              rotateAlways: false
            }
          },
          yaxis: {
            show: false
          } 
        };
        Global.runApexChart = new ApexCharts(document.querySelector("#chartPortafolio1"), oConfigApexChart);
        Global.runApexChart.render();

        // Enviar portafolio por default
        var idPortafolioDefault = _oData.datos.cuenta[0].nomcta;
        methods.setDataRentability(idPortafolioDefault)
      });

      // Cargar gráfico de evolución histórica
      Global.promisePortfolio.then(function (_oData) {
        
        // Generate grafico del Plugin        
        Highcharts.setOptions(
          {
              lang: {
                  "resetZoom":"Volver",
                  "thousandsSep": ",",
                  "months": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                  "noData": "Sin datos para mostrar",
                  "numericSymbols": ["k", "M", "B", "T", "P", "E"],
                  "shortMonths": ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                  "weekdays": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
              },
              chart:  {
                  height: 250,
                  zoomType: 'x',
                  resetZoomButton: {
                    theme: {
                        padding: 12,
                        fill: 'white',
                        style: {
                                color: '#2765F5'
                                },
                        stroke: '#2765F5',
                        r: 0,
                        states: {
                            hover: {
                                fill: '#1148C6',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    }
                  }
                },

                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                credits: {
                  enabled : false
                },
                legend: {
                  enabled: false
              },
                exporting:{
                  enabled : false
                },
                tooltip: {
                    backgroundColor: "#2765F5",
                    enabled: true,
                    headerFormat: "<span style=\"color: #fff; font-size: 14px;\">{point.key}</span><br/>",
                    followPointer: true,
                    followTouchMove: true,
                    pointFormat: "<span style=\"color:{point.color}\"></span><b style=\"color: #fff;\">{point.y}</b><br/>",
                    shadow: false,
                    shared: false,
                    useHTML: true,
                    padding: 7,
                    split: false,
                    style: {
                        color: "#fff",
                        fontSize: "14px",
                        cursor: "pointer"
                    },
                    valueDecimals: 2,
                    valuePrefix: "$",
                    valueSuffix: " USD",
                    animation: true
                },
                xAxis: {
                    type: 'datetime',
                    crosshair: {
                      "snap": true,
                      "zIndex": 7,
                      "dashStyle": "Dash",
                      "color": "#233E99",
                      "width": 1
                  },
                  events: {
                    afterSetExtremes() {
                      let chart = this.chart;
                      chart.series[0].update({
                        marker: {
                          radius: 1
                        }
                      });
                    }
                  }
                },
                yAxis: [
                  {
                    labels: {
                        format: "",
                        formatter: function() {
                            if (this.value > 999999) {
                                return Highcharts.numberFormat(this.value / 1000000, ) + "M"
                              } 
                              else if ( this.value > 999 ) {
                                return Highcharts.numberFormat( this.value/1000, 1) + "k"; 
                              } 
                            else{
                              return Highcharts.numberFormat(this.value,0);
                            }   
                          }
                    },
                    
                    type: "area",
                    crosshair: {
                        "color": "#233E99",
                        "dashStyle": "Dash",
                        "zIndex": 7
                    },
                    title: {
                          "text": ""
                      }            
                },            
              ],          
              plotOptions: {              
                  area: {
                      allowPointSelect: false,
                      fillColor: "rgba(211, 225, 252, 0.5)",
                      description: "",
                      cursor: "pointer",
                      lineWidth: 2
                }
              },
              series: [{
                  data : [],
                  marker: {
                    radius: 1
                  }
              }]
          }
        );
        
        Global.EvolHistorica = Highcharts.chart('EvolHistorica', {
          series: [{
            color: '#233E99',
            type: 'area',
            data: []
            }],
            xAxis: {
              categories: []
            }
        });


        //Enviar portafolio por default
        var idPortafolioDefault = _oData.datos.cuenta[0].nomcta;
        methods.setDataEvolutionHistoric(idPortafolioDefault)
      })

    },
    // Setear datos para pintar y actualizar las tablas de Detalle Posición
    setDataPosition : function (_idCheckeds) {
      // Aquí debo crear función para actualizar las tablas con valores detectados
      console.log(_idCheckeds)

    },    
    // Setear datos para gráfico de Rentabilidad por ID Portafolio
    setDataRentability : function (_idPortfolio){
      services.getDataRentability(function(_promise) {
        _promise.done(function(_oData) {

          // Obtener RENTABILIDAD según ID de portafolio
          var oDataRentability =  _oData.data.filter(function(_oPortfolios) {
            return _oPortfolios.portfolio == _idPortfolio;
          });
          
          // Obtener data exacto de variable RENTABILIDAD
          var _oData = oDataRentability[0].profitability;
          var oDataRentabilityNew = []
          $(_oData).each(function(i, v) {
            oDataRentabilityNew.push({
              "x": v.monthYear,
              "y": v.profitability
            });
          })
          Global.runApexChart.updateSeries([{
            name: 'monthYear',
            data: oDataRentabilityNew
          }])
        });
        _promise.always(function() {
          
        });
      });
    },
    // Setear datos para gráfico de Evolución Histórica por ID Portafolio
    setDataEvolutionHistoric :function (_idPortfolio){
      services.getDataEvolutionHistoric(function(_promise) {
        _promise.done(function(_oData) {
          
            if($('#selPortfolios').val() == '100000-1'){
              Global.EvolHistorica.series[0].update({
                data: [ 4087.7700, 22667.0200, 17364.8300, 16101.2200, 10645.4700, 9049.9100, 9888.9900, 11208.8600, 7506.7700, 10742.4000, 11050.0100, 10540.8500]
              })

              Global.EvolHistorica.xAxis[0].update({
                categories: ['Ene-15', 'Feb-15', 'Mar-15', 'Abr-15', 'May-15', 'Jun-15', 'Jul-15', 'Ago-15', 'Sep-15', 'Oct-15', 'Nov-15', 'Dic-15']
              })
            }
            if($('#selPortfolios').val() == '100000-2'){
              Global.EvolHistorica.series[0].update({
                data: [ 9193.5700, 9382.1600, 31123.3400, 32141.9400, 31030.8500, 27512.3300, 27494.4800, 25807.9300, 23643.8300, 25010.7000, 23967.4900, 24890.0800]
              })

              Global.EvolHistorica.xAxis[0].update({
                categories: ['Ene-16', 'Feb-16', 'Mar-16', 'Abr-16', 'May-16', 'Jun-16', 'Jul-16', 'Ago-16', 'Sep-16', 'Oct-16', 'Nov-16', 'Dic-16']
              })
            }
            if($('#selPortfolios').val() == '100000-3'){
              Global.EvolHistorica.series[0].update({
                data: [ 26112.7600, 16568.2800, 45084.5100, 44505.7200, 44505.9800, 19460.2300, 14510.3800, 14929.4000, 15662.4400, 14838.2200, 13712.8900, 13306.5500]
              })

              Global.EvolHistorica.xAxis[0].update({
                categories: ['Ene-17', 'Feb-17', 'Mar-17', 'Abr-17', 'May-17', 'Jun-17', 'Jul-17', 'Ago-17', 'Sep-17', 'Oct-17', 'Nov-17', 'Dic-17']
              })
            }


            
                
          
        });
        _promise.always(function() {
          
        });
      });      
    }
  }

  var initialize = function () {
    events.detectMobile()
    services.getSelectPortfolios();
    subsEvents();
  };

  return {
    init: initialize
  };
})();

window.addEventListener(
  'load',
  function () {
    card_portafolios.init();
  },
  false
);

