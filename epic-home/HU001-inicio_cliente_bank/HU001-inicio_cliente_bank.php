<?php

	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
	header("Access-Control-Allow-Headers: X-Requested-With");
	header('Content-Type: text/html; charset=utf-8');
	header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
	// Indica los métodos permitidos.
    header('Access-Control-Allow-Methods: GET, POST, DELETE');
    // Indica los encabezados permitidos.
    header('Access-Control-Allow-Headers: Authorization');
?>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="encoding" charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Access-Control-Allow-Origin" content="*">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html">
	<meta name="robots" content="all, index, follow">
	<meta name="googlebot" content="all, index, follow">
	<meta http-equiv="cache-control" content="no-cache">
	<meta property="og:type" content="website">
	<meta name="twitter:card" content="summary_large_image">
	<meta property="og:locale" content="es_ES">
	<meta property="og:locale:alternate" content="es_ES">
	<meta http-equiv="content-language" content="es_ES">
	<meta name=apple-mobile-web-app-capable content="yes">
	<meta name="owner" content="Inteligo Bank">
	<meta name="author" content="Inteligo Bank">
	<meta name="publisher" content="Inteligo Bank">
	<meta name="copyright" content="Inteligo Bank">
	<meta name="twitter:creator" content="Inteligo Bank">
	<meta name="twitter:site" content="Inteligo Bank">
	<meta name="generator" content="Inteligo Bank">
	<meta name="organization" content="Inteligo Bank">
	<meta property="og:image:width" content="598">
	<meta property="og:image:height" content="274">
	<link rel="icon" sizes="192x192" href="../../private/pages/masterpage/img/logo-inteligo-mobile.png">
	<link rel="icon" sizes="32x32" href="../../private/pages/masterpage/img/logo-inteligo-mobile.png">
	<meta property="twitter:image" content="../../private/pages/masterpage/img/logo-inteligo-mobile.png">
	<meta property="og:image" content="../../private/pages/masterpage/img/logo-inteligo-mobile.png">
	<meta property="og:image:secure_url" content="../../private/pages/masterpage/img/logo-inteligo-mobile.png">
	<link rel="apple-touch-icon-precomposed" href="../../private/pages/masterpage/img/logo-inteligo-mobile.png">
	<meta name="msapplication-TileImage" content="../../private/pages/masterpage/img/logo-inteligo-mobile.png">
	<link rel="icon" type="image/jpg" href="../../private/pages/masterpage/img/logo-inteligo-mobile.png">

	<link href="https://fonts.gstatic.com" crossorigin rel="preconnect">
	<meta name="description" content="Somos una empresa de asesoría financiera y servicios de gestión patrimonial internacional que forma parte de Intercorp, importante grupo empresarial peruano comprometido con el desarrollo y bienestar de todas las familias.">
	<meta name="twitter:description" content="Somos una empresa de asesoría financiera y servicios de gestión patrimonial internacional que forma parte de Intercorp, importante grupo empresarial peruano comprometido con el desarrollo y bienestar de todas las familias.">
	<meta property="og:description" content="Somos una empresa de asesoría financiera y servicios de gestión patrimonial internacional que forma parte de Intercorp, importante grupo empresarial peruano comprometido con el desarrollo y bienestar de todas las familias.">
	<meta property="og:title" content="Inteligo Bank">
	<meta name="twitter:title" content="Inteligo Bank">
	<meta name="dcterms.title" content="Inteligo Bank">
	<meta name="DC.title" content="Inteligo Bank">
	<meta name="application-name" content="Titulo de la página">

	<title>INICIO</title>

	<!-- General -->
	<link rel="stylesheet" href="../../private/pages/masterpage/css/masterpage.css">
	<!-- end -->

	<!-- Módulo específico -->
	<link rel="stylesheet" href="css/HU001-inicio_cliente_bank.css">
	<!-- end -->
</head>

<body>

	<!-- Contenedor principal -->
	<section class="inteligo-layout">
		
		<!-- Sidebar -->
		<?php include "../../private/organisms/sidebar/sidebar.html" ?>
		<!-- end Sidebar -->
		
		<!-- Para cuando no tenga insights, se realizar 2 ajustes  -->
		<!-- 1. En el HTML, agregar la clase  no-insighths  -->
		<!-- 2. En el JS, manipular el valor de Global.isInsights  -->
		<main id="inteligoMain" class="inteligo-main ">

			<!-- Header -->
			<?php include "../../private/organisms/header/header.html" ?>
			<!-- end Header -->

	
			<section class="inteligo-main-content">
				<!-- <div class="inteligo-main-top">
					<h1 class="inteligo-title-welcome">Bienvenido(a) a la zona segura</h1>

					<h2 class="inteligo-title-access">Último acceso: 22/10/2021 12:44</h2>
				</div> -->

					
				<!-- Sección Insigths -->
				<section id="secInsights" class="inteligo-insights">
					<?php include "_components/card-insights/insights.html" ?>	
				</section>
				<!-- end -->

				<!-- Sección Twice Cards -->
				<section id="secMainCards" class="main-components main-components-columns">
					
					<div class="main-card-row">
						<!-- Portafolio -->
						<div id="cardPortafolios" class="main-card-box main-card-portafolios ovrf-vsbl">					
							<?php include "_components/card-portafolio/portafolio.html" ?>
						</div>
						<!-- end Portafolio -->
						
						

					</div>

					<div class="main-card-row">

						<!-- Análisis -->
						<div id="cardAnalisis"  class="main-card-box main-card-analisis">
							<?php include "_components/card-analisis/analisis.html" ?>
						</div>
						<!-- end Análisis -->

					</div>
				</section>
				<!-- end -->

				<!-- Sección DetailPage -->
				<section id="secDetailpage" class="main-card-box main-card-detailpage " style="display: none;">
					<?php include "_components/card-detailpage/detailpage.html" ?>
				</section>
				<!-- end -->

				<!-- Sección CtaTrading -->
				<section class="main-card-box main-card-trading">
					<?php include "_components/cta-trading/trading.html" ?>
				</section>

				<!-- Sección CtaCorriente -->
				<section class="main-card-box main-card-corriente">
					<?php include "_components/cta-corriente/corriente.html" ?>
				</section>
				<!-- end -->

				<!-- Sección CreditCard -->
				<section id="secCreditCard" class="main-card-box main-card-creditcard " style="display: none;">
					<?php include "_components/creditcard/creditcard.html" ?>
				</section>
				<!-- end -->

				<!-- Sección Préstamos -->
				<section class="main-card-box main-card-prestamos">
					<?php include "_components/prestamos/prestamos.html" ?>
				</section>
				<!-- end -->

			</section>			
		</main>
	</section>
	<!-- End  -->	

	<!-- Menu toolbar -->
	<?php include "../../private/organisms/menutoolbar/menutoolbar.html" ?>
	<!-- end Menu toolbar -->

	<!-- Popup Intro -->
	<?php include "../../private/organisms/popup/popup-intro.html" ?>
	<?php include "../../private/organisms/popup/popup-mobile.html" ?>
	<!-- end Popup Intro -->
	
	<!-- end Popup Intro -->	

	<!-- Agregar sidebar bottom  -->
	<div id="inteligoModalMobile" class="inteligo-sidebarbottom">
		<!-- Lista de tarjetas para Modal en TC -->
		<div id="sidebarbottomTC" class="inteligo-sidebarbottom-tc">
			<div class="inteligo-sidebarbottom-tc-bg"></div>
			<div class="inteligo-sidebarbottom-content">
				<i  class="icon icon-close "></i>
				<div class="inteligo-sidebarbottom-tc-title">Seleccione su tarjeta:</div>
				<div id="selTarjetasMob" class="inteligo-sidebarbottom-tc-content">
					<div class="inteligo-sidebarbottom-tc-option">
						<label class="container-radio " for="tarjeta-credito1">
							<input type="radio" value="1" id="tarjeta-credito1" name="mob-sel-tarjetas" checked>
							<span class="checkmark"></span>
							<label for="tarjeta-credito1" class="container-radio-text">Todas mis tarjetas</label>
		    			</label>
					</div>
					<!-- Se listan aquí la lista de tarjetas -->
				</div>
			</div>
		</div>
	</div>

	<!-- Capa oscura -->
	<section id="layoutBackdrop" class="inteligo-backdrop"></section>
	<!-- end Capa oscura -->

	<!-- Block content to all modals  -->
	<div id="inteligoContentModals" class="inteligo-content-modals"></div>

	<!-- Capa para agregar dentro otros componentes -->
	<footer class="inteligo-footer"> </footer>
	<!-- end Capa -->

	<!-- PLugins y Masterpage -->
	<script type="text/javascript" src="../../static/libs/jquery/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../../private/pages/masterpage/js/masterpage.js"></script>
	<!-- Módulo específico -->
	<script type="text/javascript" src="js/HU001-inicio_cliente_bank.js"></script>
	<!-- Levantar el plugin Owl, para la Popup Intro -->
	<script type="text/javascript" src="../../static/libs/owl/owl.min.js"></script>
	

	
</body>
</html>