/*
 * Master Page
 * @author mfyance
 */
var inicio_cliente_bank = (function () {

  var methods = {
    loadLibs: function () {
      // Chart Complex
      Global.loadJS("../../static/libs/highcharts/highcharts.min.js");
      // Chart simple
      Global.loadJS("../../static/libs/chart/apexcharts.min.js");
      // NiceSelect
      Global.loadJS("../../static/libs/nice-select/script.js");
      Global.loadCSS("../../static/libs/nice-select/style.css");
      // Mustache Templates
      Global.loadJS("../../static/libs/mustache/mustache.min.js");
      // Data time picker
      Global.loadJS("../../static/libs/datepicker/bootstrap-datepicker.min.js");
      Global.loadJS("../../static/libs/datepicker/bootstrap-datepicker.es.min.js");
      Global.loadCSS("../../static/libs/datepicker/bootstrap.min.css")
      Global.loadCSS("../../static/libs/datepicker/bootstrap-datepicker.min.css")
      // Select2
      Global.loadJS("../../static/libs/select2/select2.min.js");
      Global.loadCSS("../../static/libs/select2/select2.min.css");
      // Easy Autocomplete
      Global.loadJS("../../static/libs/autocomplete/easy-autocomplete.min.js");
      Global.loadCSS("../../static/libs/autocomplete/easy-autocomplete.min.css");
      // Owl
      Global.loadCSS("../../static/libs/owl/owl.min.css")
      Global.loadCSS("../../static/libs/owl/owl.theme.min.css")
      Global.loadCSS("../../static/libs/owl/animate.min.css")
      // Air-datepicker
      Global.loadCSS("../../static/libs/datepicker/air-datepicker.css");
      Global.loadCSS("../../static/libs/datepicker/custom-datepicker.css");
      Global.loadJS("../../static/libs/datepicker/air-datepicker.js");
      
      // Moment Js
      Global.loadJS("../../static/libs/moment/moment.min.js")
      Global.loadJS("../../static/libs/moment/moment.es.min.js")
      
    },
    loadCards: function () {
      // Componente 1 : Portafolios
      Global.loadCSS("_components/card-portafolio/portafolio.css");
      Global.loadJS("_components/card-portafolio/portafolio.js");

      // Componente 2 : Abonos
      // Global.loadJS("_components/card-abonos/abonos.js");
      // Global.loadCSS("_components/card-abonos/abonos.css");

      // Componente 3 : Analisis
      Global.loadJS("_components/card-analisis/analisis.js");
      Global.loadCSS("_components/card-analisis/analisis.css");
      
      // Componente 4 : Insights
      Global.loadJS("_components/card-insights/insights.js");
      Global.loadCSS("_components/card-insights/insights.css");

      // Componente 5: Saber Más
      Global.loadJS("_components/card-detailpage/detailpage.js");
      Global.loadCSS("_components/card-detailpage/detailpage.css");

      // Componente 6: Cuenta corriente
      Global.loadJS("_components/cta-corriente/corriente.js");
      Global.loadCSS("_components/cta-corriente/corriente.css")

      // Componente 6: Cuenta trading
      Global.loadJS("_components/cta-trading/trading.js");
      Global.loadCSS("_components/cta-trading/trading.css")

      // Componente 7: Credit card
      Global.loadCSS("_components/creditcard/creditcard.css")
      Global.loadJS("_components/creditcard/creditcard.js");

      // Componente 8: Préstamos
      Global.loadCSS("_components/prestamos/prestamos.css")
      Global.loadJS("_components/prestamos/prestamos.js");

    },
    loadComponents : function(){
      // Cargar Intro popup si es necesario
      Global.loadJS("../../private/organisms/popup/popup.js");
      // Cargar de la funcionalidad dentro de la modal
      Global.loadJS("../../private/pages/modals/modal.js");
      //cargar javascript del sidebar
      Global.loadJS("../../private/organisms/sidebar/sidebar.js")
    },

    // Simulación de como se oculta los Skeleton
    testSkeleton: function () {
      
      setTimeout(function(){

        Global.hideSkeleton("#cardPortafolios")
        Global.hideSkeleton("#cardAbonos")
        Global.hideSkeleton("#cardAnalisis")

      }, 700)

    }
  };

  var initialize = function () {
    methods.loadLibs()
    methods.loadCards()
    methods.loadComponents()
    methods.testSkeleton();
  };

  return {
    init: initialize
  };
})();

document.addEventListener(
  'DOMContentLoaded',
  function () {
    inicio_cliente_bank.init();
  },
  false
);