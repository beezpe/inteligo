$(document).ready(function(){

    $('.btnChat').on('click',function(){
        $(this).toggleClass('btnChatOpen');
        $('.chatBox').toggleClass('chatBoxOpen');
        $('.chatStep').hide();
        $('.chatStep1').show();
        $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
        $('.chatStep').find('*').removeAttr('style');
        

        $('.chatStep1 .messageBox').eq(0).addClass('animate__animated animate__fadeInUp');
        setTimeout(function(){
            $('.chatStep1 .messageBox').eq(0).find('.typingBox').addClass('animate__animated animate__fadeOut');
        },1000);
        setTimeout(function(){
            $('.chatStep1 .messageBox').eq(0).find('.txt').addClass('animate__animated animate__fadeIn');
        },1300);
        setTimeout(function(){
            $('.chatStep1 .messageBox').eq(1).addClass('animate__animated animate__fadeIn');
        },1100);
        setTimeout(function(){
            $('.chatStep1 .messageBox').eq(1).find('.typingBox').addClass('animate__animated animate__fadeOut');
        },2500);
        setTimeout(function(){
            $('.chatStep1 .messageBox').eq(1).find('.txt').addClass('animate__animated animate__fadeIn');
        },2800);
        setTimeout(function(){
            $('.chatStep1 .messageOptionBox').addClass('animate__animated animate__fadeIn');
        },3500);

    });


    $('.chatClose').on('click',function(){
        $('.chatBox').toggleClass('chatBoxOpen');
        $('.btnChat').toggleClass('btnChatOpen');
        $('.chatStep').hide();
        $('.chatStep1').show();
        $('.chatTitle').hide();
        $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
        $('.chatStep').find('*').removeAttr('style');
        
        
    });

    $('.messageOption1').on('click',function(){
        $('.chatStep').hide();
        $('.chatStep2').show();
        $('.chatTitle1').show();
        $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
        $('.chatStep').find('*').removeAttr('style');
        
        $('.chatStep2 .messageBox').eq(0).css('opacity',1);
        $('.chatStep2 .messageBox').eq(0).find('.txt').css('opacity',1);
        setTimeout(function(){
            $('.chatStep2 .formStep').addClass('animate__animated animate__fadeIn')
        },200);
    });

    $('.messageOption2').on('click',function(){
        $('.chatStep').hide();
        $('.chatStep3').show();
        $('.chatTitle1').show();
        $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
        $('.chatStep').find('*').removeAttr('style');
        
        $('.chatStep3 .messageBox').eq(0).css('opacity',1);
        $('.chatStep3 .messageBox').eq(0).find('.txt').css('opacity',1);
        setTimeout(function(){
            $('.chatStep3 .formStep').addClass('animate__animated animate__fadeIn')
        },200);
    });

    $('form').submit(function(e){
        e.preventDefault();
    });

    // validate signup form on keyup and submit
    var formStep1 = $("#formStep1");
    formStep1.validate({
        errorElement: "small",
        rules: {
            email_form1: {
                required: true,
                email: true
            },
            affair_form1: "required",
            message_form1: "required",
        },
        submitHandler: function(){
            $('.chatStep').hide();
            $('.chatStep4').show();
            $('.chatTitle').hide();
            $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
            $('.chatStep').find('*').removeAttr('style');
            
            $('.chatStep4 .thanksBox').addClass('animate__animated animate__fadeIn')
            
        },
        highlight: function(element) {
            $(element).parent('div').addClass('error');
        },
        unhighlight: function(element) {
            $(element).parent('div').removeClass('error');
        },
        messages:{
            email_form1:'Ingrese un correo valido por favor.',
            affair_form1:'Seleccione el asunto por favor.',
            message_form1:'Deje un mensaje por favor.',
        },
    });

    var formStep2 = $("#formStep2");
    formStep2.validate({
        errorElement: "small",
        rules: {
            phone_form2: {
                required: true,
                minlength: 6,
                maxlength: 14,
                number:true
            },
            affair_form2: "required",
            message_form2: "required",
        },
        submitHandler: function(){
            $('.chatStep').hide();
            $('.chatStep4').show(); 
            $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
            $('.chatStep').find('*').removeAttr('style');
            
            $('.chatStep4 .thanksBox').addClass('animate__animated animate__fadeIn')
        },
        highlight: function(element) {
            $(element).parent('div').addClass('error');
        },
        unhighlight: function(element) {
            $(element).parent('div').removeClass('error');
        },
        messages:{
            phone_form2:'Ingrese un teléfono valido por favor.',
            affair_form2:'Seleccione el asunto por favor.',
            message_form2:'Deje un mensaje por favor.',
        },
    });


    $('.btnThanks1').on('click',function(){
        $('.chatStep').hide();
        $('.chatStep5').show(); 
        $('.chatTitle').hide();
        $('.chatTitle2').show();
        $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
        $('.chatStep').find('*').removeAttr('style');
        
        $('.chatStep5 .messageBox').eq(0).addClass('animate__animated animate__fadeIn')
        
        setTimeout(function(){
            $('.chatStep5 .messageBox').eq(0).find('.typingBox').addClass('animate__animated animate__fadeOut');
        },800);
        setTimeout(function(){
            $('.chatStep5 .messageBox').eq(0).find('.txt').addClass('animate__animated animate__fadeIn');
        },1000);

        setTimeout(function(){
            $('.chatStep5 .formStep').addClass('animate__animated animate__fadeIn')
        },1700);
    });


    var formStep3 = $("#formStep3");
    formStep3.validate({
        errorElement: "small",
        rules: {
            message_form3: "required",
        },
        submitHandler: function(){
            $('.chatBox').toggleClass('chatBoxOpen');
            $('.chatStep').hide();
            $('.chatStep1').show();
            $('.btnChat').toggleClass('btnChatOpen');
            $('.chatTitle').hide();
            $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
            $('.chatStep').find('*').removeAttr('style');
            

        },
        messages:{
            message_form3:'Deje un mensaje por favor.',
        },
    });

    $('.btnThanks2').on('click',function(){
        $('.chatBox').toggleClass('chatBoxOpen');
        $('.chatStep').hide();
        $('.chatStep1').show();
        $('.btnChat').toggleClass('btnChatOpen');
        $('.chatTitle').hide();
    });

    $('.chatTitle1').on('click',function(){
        $('.chatStep').hide();
        $('.chatStep1').show();
        $('.chatTitle').hide();
        $('.chatBox').find('.animate__animated').removeClass('animate__animated animate__fadeInUp animate__fadeIn animate__fadeOut');
        $('.chatStep').find('*').removeAttr('style');
        


        $('.chatStep1 .messageBox').eq(0).css('opacity',1);
        $('.chatStep1 .messageBox').eq(0).find('.txt').css('opacity',1);
        $('.chatStep1 .messageBox').eq(0).find('.typingBox').css('opacity',0);

        $('.chatStep1 .messageBox').eq(1).css('opacity',1);
        $('.chatStep1 .messageBox').eq(1).find('.txt').css('opacity',1);
        $('.chatStep1 .messageBox').eq(1).find('.typingBox').css('opacity',0);

        $('.chatStep1 .messageOptionBox').css('opacity',1);
        

    })

});

//material contact form animation
var floatingField = $('.material-form .floating-field').find('.form-control');
var formItem = $('.material-form .input-block').find('.form-control');

//##case 1 for default style
//on focus
formItem.focus(function() {
  $(this).parent('.input-block').addClass('focus');
});
//removing focusing
formItem.blur(function() {
  $(this).parent('.input-block').removeClass('focus');
});

//##case 2 for floating style
//initiating field
floatingField.each(function() {
  var targetItem = $(this).parent();
  if ($(this).val()) {
    $(targetItem).addClass('has-value');
  }
});

//on typing
floatingField.blur(function() {
  $(this).parent('.input-block').removeClass('focus');
  //if value is not exists
  if ($(this).val().length == 0) {
    //$(this).parent('.input-block').removeClass('has-value');
  }else{
    //$(this).parent('.input-block').addClass('has-value');
  }
});

//dropdown list
$('body').click(function() {
  if ($('.custom-select .drop-down-list').is(':visible')) {
    //$('.custom-select').parent().removeClass('focus');
  }
  $('.custom-select .drop-down-list:visible').slideUp();
});
$('.custom-select .active-list').click(function() {
  $(this).parent().parent().addClass('focus');
  $(this).parent().find('.drop-down-list').stop(true, true).delay(10).slideToggle(300);
});
$('.custom-select .drop-down-list li').click(function() {
  var listParent = $(this).parent().parent();
  //listParent.find('.active-list').trigger("click");
  listParent.parent('.select-block').removeClass('focus').addClass('added');
  listParent.find('.active-list').text($(this).text());
  listParent.find('input.list-field').attr('value', $(this).text());
  $(this).closest('.custom-select').removeClass('error');
  $(this).closest('.custom-select').find('small').hide();
});