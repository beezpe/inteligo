$(document).ready(function(){

    // validate signup form on keyup and submit
    var formCheckout = $("form");
    formCheckout.validate({
        errorElement: "small",
        rules: {
            email_field: {
                required: true,
                email: true
            },
            phone_field: {
                required: true,
                minlength: 6,
                maxlength: 14,
                number:true
            },
            affair_field: "required",
            message_field: "required",
        },
        submitHandler: function(){

        },
        highlight: function(element) {
            $(element).parent('div').addClass('error');
        },
        unhighlight: function(element) {
            $(element).parent('div').removeClass('error');
        },
        messages:{
            email_field:'Ingrese un correo valido por favor.',
            phone_field:'Ingrese un teléfono valido por favor.',
            affair_field:'Ingrese el asunto por favor.',
            message_field:'No olvide escribir su mensaje.',
        },
    });

    $('.emptyField').hide();
    $('#message_field').on('keyup',function(){
        var maxLenghtText = 150;
        $('.caracteresBox span').empty();
        $('.caracteresBox span').html(150 - this.value.length)
        
        if(this.value.length > 3){
            $('.emptyField').show();
        }
    });

    $('.emptyField').on('click',function(){
        $('#message_field').val('');
        $('.caracteresBox span').html('150');
        $('.emptyField').hide();
    });


});


//material contact form animation
var floatingField = $('.material-form .floating-field').find('.form-control');
var formItem = $('.material-form .input-block').find('.form-control');

//##case 1 for default style
//on focus
formItem.focus(function() {
  $(this).parent('.input-block').addClass('focus');
});
//removing focusing
formItem.blur(function() {
  $(this).parent('.input-block').removeClass('focus');
});

//##case 2 for floating style
//initiating field
floatingField.each(function() {
  var targetItem = $(this).parent();
  if ($(this).val()) {
    $(targetItem).addClass('has-value');
  }
});

//on typing
floatingField.blur(function() {
  $(this).parent('.input-block').removeClass('focus');
  //if value is not exists
  if ($(this).val().length == 0) {
    $(this).parent('.input-block').removeClass('has-value');
  }else{
    $(this).parent('.input-block').addClass('has-value');
  }
});

//dropdown list
$('body').click(function() {
  if ($('.custom-select .drop-down-list').is(':visible')) {
    $('.custom-select').parent().removeClass('focus');
  }
  $('.custom-select .drop-down-list:visible').slideUp();
});
$('.custom-select .active-list').click(function() {
  $(this).parent().parent().addClass('focus');
  $(this).parent().find('.drop-down-list').stop(true, true).delay(10).slideToggle(300);
});
$('.custom-select .drop-down-list li').click(function() {
  var listParent = $(this).parent().parent();
  //listParent.find('.active-list').trigger("click");
  listParent.parent('.select-block').removeClass('focus').addClass('added');
  listParent.find('.active-list').text($(this).text());
  listParent.find('input.list-field').attr('value', $(this).text());
  $(this).closest('.custom-select').removeClass('error');
  $(this).closest('.custom-select').find('small').hide();
});